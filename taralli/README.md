# Framework

We provide a unified interface to multiple commonly used components but they are separated as much as possible
- statistical parameter estimation
- likelihood function construction
- surrogating
- post-processing
- reliability analysis

# Inspiration
* keras
* sklearn
* **nestle**, dynesty
* eemcee

* stan
* pyMC3
* Edward

# notes

* options for various model definitions
* print informative messages to the console, e.g. default settings such as solver if not explicitely specified + maybe use logging to write to a file -> for inspection/debugging
* talk with the programmer guy from UvA

* get inspiration from: https://github.com/UCL-CCS/EasyVVUQ
* Quality control: https://muscle3.readthedocs.io/en/latest/devtools.html
* unit test
* continuous integration
* seems to be a quite good practice: https://muscle3.readthedocs.io/en/latest/devtools.html

* shall we anticipate cases where the likelihood is not surrogated? -> user's responsibility to wrap the third party code into a likelihood -> we can have smarter options -> not pre-surrogated likelihoods
* how to deal with GPR surrogate model uncertianty in the inference?
