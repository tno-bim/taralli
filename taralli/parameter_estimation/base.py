# Base classes

import logging
import random
import time
from typing import Callable, Optional, Union

import dynesty
import emcee
import nestle
import numpy as np
from dynesty import utils as dyfunc
from tabulate import tabulate

from taralli.parameter_estimation import (
    DYNESTY_DYNAMIC_KWARGS,
    DYNESTY_STATIC_KWARGS,
)
from taralli.parameter_estimation.visualization import visualize_marginal_matrix
from taralli.parameter_estimation.weighted_sample_statistics import (
    weighted_sample_covariance,
    weighted_sample_mean,
    weighted_sample_quantile,
)
from taralli.utilities.output import pretty_time_delta


# TODO:
#   - Dynesty:
#       - vectorization/parallelization


class StatModel:
    """
    Base class to perform Bayesian parameter estimation.

    At the moment the parameter estimation can be performed using nested sampling
    with the `NestleParameterEstimator` class and using Markov chain Monte Carlo
    method with the `EmceeParameterEstimator` class.
    TODO:
     * This seems to be an abstract class (never to be used directly), python's
     existing machinery could be used for this.

    """

    def __init__(self):
        self.parameter_estimation_output = None
        self.posterior_sample = None
        self.posterior_sample_weights = None
        self.posterior_sample_log_evidence = None
        self.posterior_estimation_output = None
        self.prior_sample = None
        self.prior_sample_weights = None
        self.prior_sample_log_evidence = None
        self.prior_estimation_output = None
        self.summary_output = None
        self.kl_divergence = None

    def plot_prior(self, **kwargs):
        """
        Sample the prior distribution and plots the 1D and 2D marginal in a
        matrix/triangular plot.

        Args:
            **kwargs: key-words argument to customize the plot. See
                `visualize_marginal_matrix` for details.

        Returns:
            fig: triangular plot.
            theta_range_on_plot: range of the parameter(s) to be estimated.

        """
        (
            self.prior_sample,
            self.prior_sample_weights,
            self.prior_sample_log_evidence,
        ) = self._sample_prior()

        fig, theta_range_on_plot = visualize_marginal_matrix(
            sample=self.prior_sample, weight_vec=self.prior_sample_weights, **kwargs
        )
        return fig, theta_range_on_plot

    def plot_posterior(self, **kwargs):
        """
        Plots the posterior distribution`s 1D and 2D marginal in a triangular plot.

        Args:
            **kwargs: key-words argument to customize the plot. See
                `visualize_marginal_matrix` for details.

        Returns:
            fig: triangular plot.
            theta_range_on_plot: range of the estimated parameter(s).

        """
        fig, theta_range_on_plot = visualize_marginal_matrix(
            sample=self.posterior_sample,
            weight_vec=self.posterior_sample_weights,
            **kwargs,
        )
        return fig, theta_range_on_plot

    def summary(self):
        """
        Compute and print a summary of the posterior distribution containing
            `mean`, `median`, `standard deviation`, `25th percentile`,
            `75th percentile`, and `95th percentile`.

        In `self.summary_output` a short version of the summary is stored containing
        `mean`, `median`, and `covariance`.

        """

        mean = weighted_sample_mean(
            self.posterior_sample, self.posterior_sample_weights
        )

        quantiles = weighted_sample_quantile(
            self.posterior_sample,
            np.array([0.25, 0.50, 0.75, 0.95]),
            self.posterior_sample_weights,
        )

        keys = ["mean", "median", "covariance"]
        summary = dict.fromkeys(keys)

        summary["median"] = quantiles[1, :]
        summary["covariance"] = weighted_sample_covariance(
            self.posterior_sample, self.posterior_sample_weights
        )
        summary["mean"] = mean

        self.summary_output = summary

        col_names = ["", "mean", "median", "sd", "25%", "75%", "95%"]
        param = np.array([f"  θ_{ii + 1}" for ii in range(mean.shape[0])]).reshape(
            -1, 1
        )
        tab = np.hstack(
            (
                param,
                summary["mean"].reshape(-1, 1),
                quantiles[1, :].reshape(-1, 1),
                np.sqrt(np.diag(summary["covariance"])).reshape(-1, 1),
                quantiles[0, :].reshape(-1, 1),
                quantiles[2, :].reshape(-1, 1),
                quantiles[3, :].reshape(-1, 1),
            )
        )

        print(tabulate(tab, headers=col_names, floatfmt=".2f"))


class EmceeParameterEstimator(StatModel):
    """
    A Markov chain Monte Carlo parameter estimator. It facilitates the use of the
    python package `emcee`.

    Args:
        nwalkers: number of walkers used by the estimator.
        ndim: number of dimensions in the parameter space.
        log_prior: function that takes a vector in the parameter space as input and
            returns the natural logarithm of the prior probability distribution for
            that position.
        log_likelihood: function that takes a vector in the parameter space as input
            and returns the natural logarithm of the likelihood function.
        sampling_initial_positions: initial positions of the walkers. It is a matrix
            with `ndim` columns and `nwalkers` rows.
        nsteps: number of steps to run..
        initial_nsteps: number of steps to run in the burn-in phase..

    _Note:_ For full details on the `emcee` library see:
        https://emcee.readthedocs.io/en/stable/
    """

    def __init__(
        self,
        nwalkers: int,
        ndim: int,
        log_prior: Callable[[Union[np.ndarray, float]], Union[np.ndarray, float]],
        log_likelihood: Callable[[Union[np.ndarray, float]], Union[np.ndarray, float]],
        sampling_initial_positions: Union[np.ndarray, list],
        nsteps: int = 1_000,
        initial_nsteps: int = 100,
        seed: Optional[float] = None,
        show_sampling_progress: bool = True,
    ):

        super().__init__()
        self.nwalkers = nwalkers
        self.ndim = ndim
        self.log_prior = log_prior
        self.log_likelihood = log_likelihood
        self.log_prob_fn = lambda x: log_prior(x) + log_likelihood(x)
        self.sampling_initial_positions = np.atleast_2d(sampling_initial_positions)
        self.initial_nsteps = initial_nsteps
        self.nsteps = nsteps
        self.seed = seed
        self.show_sampling_progress = show_sampling_progress

    def estimate_parameters(self, **kwargs):
        """
        Samples the posterior distribution and stores the full results in
        `self.estimation_output`, the samples of the posterior are stored in
        `self.posterior_sample`.

        Args:
            **kwargs: key-words argument of the `emcee.EnsembleSampler()` class. For
                details:
                https://emcee.readthedocs.io/en/stable/user/sampler/#emcee.EnsembleSampler

        """
        # ..............................................................................
        # Pre-process
        # ..............................................................................
        if self.sampling_initial_positions.shape[1] != self.ndim:
            raise ValueError(
                f"`sampling_initial_positions` should have {self.ndim} columns (one "
                f"for each parameter of interest), but "
                f"{self.sampling_initial_positions.shape[1]} are provided."
            )

        if self.sampling_initial_positions.shape[0] != self.nwalkers:
            raise ValueError(
                f"`sampling_initial_positions` should have {self.nwalkers} rows (one "
                f"for each walker), but"
                f"{self.sampling_initial_positions.shape[0]} are provided."
            )

        nwalkers = self.nwalkers
        ndim = self.ndim
        log_prob_fn = self.log_prob_fn
        initial_nsteps = self.initial_nsteps
        nsteps = self.nsteps
        show_sampling_progress = self.show_sampling_progress
        sampling_initial_positions = self.sampling_initial_positions

        random.seed(self.seed)
        np.random.seed(self.seed)
        rstate = np.random.mtrand.RandomState(self.seed)

        sampler = emcee.EnsembleSampler(
            nwalkers=nwalkers,
            ndim=ndim,
            log_prob_fn=log_prob_fn,
            **kwargs,
        )

        sampler.random_state = rstate

        # ..............................................................................
        # Initial sampling, burn-in: used to avoid a poor starting point
        # ..............................................................................
        start = time.time()
        state = sampler.run_mcmc(
            initial_state=sampling_initial_positions,
            nsteps=initial_nsteps,
            progress=show_sampling_progress,
        )
        end = time.time()
        logging.info(
            f"Initial sampling is complete: {initial_nsteps} steps and {nwalkers} "
            f"walkers.\n Total run-time: {pretty_time_delta(end - start)}."
        )
        sampler.reset()

        # ..............................................................................
        # Sampling of the posterior
        # ..............................................................................
        start = time.time()
        sampler.run_mcmc(
            initial_state=state, nsteps=nsteps, progress=show_sampling_progress
        )
        end = time.time()
        logging.info(
            f"Sampling of the posterior distribution is complete: {nsteps} steps and"
            f" {nwalkers} walkers.\n Total run-time: {pretty_time_delta(end - start)}."
        )

        # ..............................................................................
        # Post-process
        # ..............................................................................
        self.parameter_estimation_output = sampler
        self.posterior_sample = sampler.get_chain(flat=True)
        self.posterior_sample_weights = None
        self.posterior_sample_log_evidence = None

    def _sample_prior(self, **kwargs):
        """
        Sample the prior distribution from `log_prior`.

        Args:
            **kwargs: key-words argument of the `emcee.EnsembleSampler()` class. For
                details:
                https://emcee.readthedocs.io/en/stable/user/sampler/#emcee.EnsembleSampler

        Returns
            prior_sample: samples of the prior distribution.
            prior_sample_weights: weights of the samples.
        """

        sampler_prior = emcee.EnsembleSampler(
            nwalkers=self.nwalkers, ndim=self.ndim, log_prob_fn=self.log_prior, **kwargs
        )
        state = sampler_prior.run_mcmc(
            initial_state=self.sampling_initial_positions,
            nsteps=self.initial_nsteps,
            progress=False,
        )
        sampler_prior.reset()
        sampler_prior.run_mcmc(state, self.nsteps)

        self.prior_estimation_output = sampler_prior
        prior_sample = sampler_prior.get_chain(flat=True)
        prior_sample_weights = None
        prior_sample_log_evidence = None
        return prior_sample, prior_sample_weights, prior_sample_log_evidence


class NestleParameterEstimator(StatModel):
    """
    A nested parameter estimator. It facilitates the use of the python library `nestle`.

    Args:
        ndim: number of dimensions.
        prior_transform:
        log_likelihood:

    _Note:_ For full details on the `nestle` library see:
        http://kylebarbary.com/nestle/index.html.
    """

    def __init__(
        self,
        ndim: int,
        prior_transform: Callable[[Union[np.ndarray, float]], Union[np.ndarray, float]],
        log_likelihood: Callable[[Union[np.ndarray, float]], Union[np.ndarray, float]],
        seed: float = None,
    ):

        super().__init__()
        self.ndim = ndim
        self.prior_transform = prior_transform
        self.log_likelihood = log_likelihood
        self.random_state = np.random.RandomState(seed)

    def estimate_parameters(self, **kwargs):
        """
        Sample the posterior distribution and stores the full results in
        `self.estimation_output`, the samples of the posterior are stored in
        `self.posterior_sample` and the relative weights in
        `self.posterior_sample_weights`.

        Args:
            **kwargs: key-words argument of the `nestle.sample()` function. For
                details: http://kylebarbary.com/nestle/api.html#nestle.sample.

        """
        parameter_estimation_output = nestle.sample(
            loglikelihood=self.log_likelihood,
            ndim=self.ndim,
            prior_transform=self.prior_transform,
            rstate=self.random_state,
            **kwargs,
        )
        self.parameter_estimation_output = parameter_estimation_output
        self.posterior_sample = parameter_estimation_output.samples
        self.posterior_sample_weights = parameter_estimation_output.weights
        self.posterior_sample_log_evidence = parameter_estimation_output.logz
        self.kl_divergence = parameter_estimation_output.h

    def _sample_prior(self, **kwargs):
        """
        Sample the prior distribution from `prior_transform`.

        Args:
            **kwargs: key-words argument of the `nestle.sample()` function. For
                details: http://kylebarbary.com/nestle/api.html#nestle.sample.

        Returns:
            prior_sample: samples of the prior distribution.
            prior_sample_weights: weights of the samples.
        """

        def fake_log_likelihood(theta):
            return np.ones((theta.shape[0], self.ndim), dtype=np.float64)

        prior_estimation_output = nestle.sample(
            loglikelihood=fake_log_likelihood,
            ndim=self.ndim,
            prior_transform=self.prior_transform,
            **kwargs,
        )

        self.prior_estimation_output = prior_estimation_output
        prior_sample = prior_estimation_output.samples
        prior_sample_weights = prior_estimation_output.weights
        prior_sample_log_evidence = prior_estimation_output.logz
        return prior_sample, prior_sample_weights, prior_sample_log_evidence


class DynestyParameterEstimator(StatModel):
    """
    A static and dynamic nested parameter estimator. It facilitates the use of the
    python package `dynesty`. The default is set to a static parameter estimator.

    Args:
        ndim: number of dimensions.
        prior_transform: prior transform function.
        log_likelihood: log_likelihood function.
        **kwargs, e.g. estimation: it can be "static", "dynamic" or "combined". The
            default is "combined".

    _Note:_ For full details on the `dynesty` library see:
        https://dynesty.readthedocs.io/en/latest/index.html.
    """

    def __init__(
        self,
        ndim: int,
        prior_transform: Callable[[Union[np.ndarray, float]], Union[np.ndarray, float]],
        log_likelihood: Callable[[Union[np.ndarray, float]], Union[np.ndarray, float]],
        seed: Optional[float] = None,
        estimation_method: Optional[str] = "static",
    ):

        super().__init__()
        self.ndim = ndim
        self.prior_transform = prior_transform
        self.log_likelihood = log_likelihood
        self.random_state = np.random.RandomState(seed)
        self.estimation_method = estimation_method

    def estimate_parameters(self, print_progress: bool = True, **kwargs):
        """
        Sample the posterior distribution and stores the full results in
        `self.estimation_output`, the samples of the posterior are stored in
        `self.posterior_sample` and the relative weights in
        `self.posterior_sample_weights`.

        Args:
            print_progress: if True prints the progress_bar. The default is True.
            **kwargs: key-words argument of function "run_nested" of the super
                classes `NestedSampler()` and DynamicNestedSampler(). For details:
                https://dynesty.readthedocs.io/en/latest/api.html#module-dynesty.dynesty

        """

        # Filtering kwargs for the `static` and for the `dynamic` estimation method.
        kwargs_keys = set(kwargs.keys())

        intersection_kwargs_static = kwargs_keys & DYNESTY_STATIC_KWARGS
        intersection_kwargs_dynamic = kwargs_keys & DYNESTY_DYNAMIC_KWARGS

        kwargs_static = {key: kwargs[key] for key in intersection_kwargs_static}
        kwargs_dynamic = {key: kwargs[key] for key in intersection_kwargs_dynamic}

        # Running the analysis according to the selected estimation_method

        if self.estimation_method == "dynamic":
            sampler = dynesty.DynamicNestedSampler(
                loglikelihood=self.log_likelihood,
                prior_transform=self.prior_transform,
                ndim=self.ndim,
                rstate=self.random_state,
            )
            sampler.run_nested(print_progress=print_progress, **kwargs_dynamic)
            parameter_estimation_output = sampler.results

        elif self.estimation_method == "combined":
            sampler = dynesty.NestedSampler(
                loglikelihood=self.log_likelihood,
                prior_transform=self.prior_transform,
                ndim=self.ndim,
            )
            dynamic_sampler = dynesty.DynamicNestedSampler(
                loglikelihood=self.log_likelihood,
                prior_transform=self.prior_transform,
                ndim=self.ndim,
            )
            sampler.run_nested(print_progress=print_progress, **kwargs_static)
            dynamic_sampler.run_nested(print_progress=print_progress, **kwargs_dynamic)
            static_results = sampler.results
            dynamic_results = dynamic_sampler.results
            parameter_estimation_output = dyfunc.merge_runs(
                [static_results, dynamic_results]
            )
        else:
            if self.estimation_method != "static":
                logging.info(
                    f"The estimation_method '{self.estimation_method}' is not "
                    f"available. The parameter estimation is run with the 'static' "
                    f"method. \n For more info see the documentation of the "
                    f"DynestyParameterEstimator() class."
                )
            sampler = dynesty.NestedSampler(
                loglikelihood=self.log_likelihood,
                prior_transform=self.prior_transform,
                ndim=self.ndim,
                rstate=self.random_state,
            )
            sampler.run_nested(print_progress=print_progress, **kwargs_static)
            parameter_estimation_output = sampler.results

        self.parameter_estimation_output = parameter_estimation_output
        self.posterior_sample = parameter_estimation_output.samples
        self.posterior_sample_weights = np.exp(
            parameter_estimation_output.logwt - parameter_estimation_output.logz[-1]
        )  # normalized weights
        self.posterior_sample_log_evidence = parameter_estimation_output.logz
        self.kl_divergence = parameter_estimation_output.information

    def _sample_prior(self, print_progress: bool = True, **kwargs):
        """
        Samples the prior distribution from `prior_transform`.
        Args:
            print_progress: if True prints the progress_bar. The default is True.
            **kwargs: key-words argument of function "run_nested" of the super
            classes `NestedSampler()` and `DynamicNestedSampler()`. For details:
            https://dynesty.readthedocs.io/en/latest/api.html#module-dynesty.dynesty.

        Returns
            prior_sample: samples of the prior distribution.
            prior_sample_weights: weights of the samples.
            prior_sample_log_evidence: natural logarithm of evidence.
        """

        def fake_log_likelihood(theta):
            return np.ones((theta.shape[0], self.ndim), dtype=np.float64)[0][0]

        # Filtering kwargs for the `static` and for the `dynamic` estimation method.
        kwargs_keys = set(kwargs.keys())

        intersection_kwargs_static = kwargs_keys & DYNESTY_STATIC_KWARGS
        intersection_kwargs_dynamic = kwargs_keys & DYNESTY_DYNAMIC_KWARGS

        kwargs_static = {key: kwargs[key] for key in intersection_kwargs_static}
        kwargs_dynamic = {key: kwargs[key] for key in intersection_kwargs_dynamic}

        # Running the analysis according to the selected `estimation_method`
        if self.estimation_method == "dynamic":
            sampler = dynesty.DynamicNestedSampler(
                loglikelihood=fake_log_likelihood,
                prior_transform=self.prior_transform,
                ndim=self.ndim,
                rstate=self.random_state,
            )
            sampler.run_nested(print_progress=print_progress, **kwargs_dynamic)
            prior_estimation_output = sampler.results

        elif self.estimation_method == "combined":
            sampler = dynesty.NestedSampler(
                loglikelihood=fake_log_likelihood,
                prior_transform=self.prior_transform,
                ndim=self.ndim,
                rstate=self.random_state,
            )
            dynamic_sampler = dynesty.DynamicNestedSampler(
                loglikelihood=fake_log_likelihood,
                prior_transform=self.prior_transform,
                ndim=self.ndim,
                rstate=self.random_state,
            )
            sampler.run_nested(print_progress=print_progress, **kwargs_static)
            dynamic_sampler.run_nested(print_progress=print_progress, **kwargs_dynamic)
            static_results = sampler.results
            dynamic_results = dynamic_sampler.results
            prior_estimation_output = dyfunc.merge_runs(
                [static_results, dynamic_results]
            )

        else:
            if self.estimation_method != "static":
                logging.info(
                    f"The estimation_method '{self.estimation_method}' is not "
                    f"available. The prior sampling is run with the 'static' method. \n"
                    f" For more info see the documentation of the "
                    f"`DynestyParameterEstimator()` class."
                )
            sampler = dynesty.NestedSampler(
                loglikelihood=fake_log_likelihood,
                prior_transform=self.prior_transform,
                ndim=self.ndim,
                rstate=self.random_state,
                **kwargs,
            )
            sampler.run_nested(print_progress=True, **kwargs_static)
            prior_estimation_output = sampler.results

        self.prior_estimation_output = prior_estimation_output
        prior_sample = prior_estimation_output.samples
        prior_sample_weights = np.exp(
            prior_estimation_output.logwt - prior_estimation_output.logz[-1]
        )  # normalized weights
        prior_sample_log_evidence = prior_estimation_output.logz
        return prior_sample, prior_sample_weights, prior_sample_log_evidence
