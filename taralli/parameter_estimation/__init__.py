"""This module performs Bayesian parameter estimation."""


# TODO: probably it would be better to pull these out of the dynetsy functions using
#  function inspection.
DYNESTY_STATIC_KWARGS = {
    "maxiter",
    "maxcall",
    "dlogz",
    "logl_max",
    "n_effective",
    "add_live",
    "print_func",
    "save_bounds",
}

DYNESTY_DYNAMIC_KWARGS = {
    "nlive_init",
    "maxiter_init",
    "maxcall_init",
    "dlogz_init",
    "logl_max_init",
    "n_effective_init",
    "nlive_batch",
    "wt_function",
    "wt_kwargs",
    "maxiter_batch",
    "maxcall_batch",
    "maxiter",
    "maxcall",
    "maxbatch",
    "n_effective",
    "stop_function",
    "stop_kwargs",
    "use_stop",
    "save_bounds",
    "print_func",
    "live_points",
}
