"""Visualize Bayesian parameter estimation results.
    * matrix plot of the 1D and 2D marginals.

TODO
  * trace plots (see bridge 705).
  * other diagnostic plots.
  * homogeneous color density.
"""

import logging
import warnings
from typing import Iterable, Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D

from taralli.parameter_estimation.utils import get_credible_region_1d
from taralli.parameter_estimation.utils import get_credible_region_2d
from taralli.parameter_estimation.weighted_sample_statistics import (
    weighted_sample_quantile,
)
from taralli.utilities.custom_error import ConvergenceError
from taralli.utilities.kde import kde_1d, kde_2d


def visualize_marginal_matrix(
    sample: np.ndarray,
    prob_mass_for_credible_region: Union[np.ndarray, list, Iterable, float] = 0.90,
    weight_vec: Optional[Union[np.ndarray, list]] = None,
    prob_mass_for_plotting_range: Union[float, np.array] = 0.9999,
    save_fig: bool = False,
    plotting_range: Optional[Union[np.ndarray, list]] = None,
    ground_truth: Optional[Union[np.ndarray, list]] = None,
    display_legend: bool = True,
    dim_labels: list = None,
    title: str = "",
    n_dimension_limit: int = 10,
    kde_1d_n_x_vec: int = 2 ** 14,
    kde_2d_n_row_mx: int = 2 ** 8,
    **kwargs,
) -> Tuple[Tuple, np.ndarray]:
    """Visualize the 1D and 2D marginals of a K-dimensional (optionally weighted)
    sample using kernel density estimates.

    If the resolution of the 1D or 2D kernel density function is low (e.g. pixelated
    2D kernel density) then by increasing `kde_1d_n_x_vec` or `kde_2d_n_row_mx`
    respectively is expected to solve the problem at the cost of increased compute time.

    Args:
        sample: array to be plotted of dimension [NxK] where N is the number of
            samples and K the number of parameters.
        prob_mass_for_credible_region: probability mass or masses to calculate
            highest density credible regions for the 1D and 2D marginals.
        weight_vec: weight_vec of sample points. This must have the same number of
            elements as rows in `sample`, the same weight is applied to all K
            coordinates of the same sample point. If `None` (default), the samples
            are assumed to be equally weighted. The absolute value of the elements of
            `weight_vec` does not matter, only the values of elements relative to
            each other, i.e. multiplying `weight_vec` by a scalar does not change the
            results.
        prob_mass_for_plotting_range: probability mass for the equal-tail credible
            region of each 1D marginal. It is used to get the boundaries of the
            plotting region (hypercube). If `plotting_range` is not `None` then
            this argument is ignored.
        save_fig: option to automatically save the produced figure.
        plotting_range: support range for plotting, shape [2, K].
        ground_truth: true value, shape [1,K].
        display_legend: option to remove the automatically generated legend.
        dim_labels: labels/names of the dimensions/parameters of interest, shape [K].
        title: figure title.
        n_dimension_limit: maximum number of dimensions that are plotted, if more
            dimensions are provided only the first `n_dimension_limit` will be plotted.
            number of points along each dimension (same for columns) where the
        kde_1d_n_x_vec: the number of `x_vec` points used in the uniform
            discretization of the interval `[x_min, x_max]`. For further details see
            the `kde_1d` function.
        kde_2d_n_row_mx: estimate of the density will be returned, i.e. total number
            of points is `n_row_x_mx**2`. For further details see the `kde_2d`
            function. Increasing this value will make the 2D color filling smoother
            at the cost of increased compute time.
        **kwargs: key arguments that are passed on to `plt.savefig()`.

    Returns:
        axes: triangular plot.
        plotting_range: support range for plotting, shape [2, K].
    """
    # ----------------------------------------------------------------------------------
    # PRE-PROCESSING
    # ----------------------------------------------------------------------------------
    color_line_1d = "#08519c"
    colors = np.array(
        [
            ["#9ECAE1"],
            ["#deebf7", "#9ECAE1"],
            ["#eff3ff", "#bdd7e7", "#6baed6"],
            ["#eff3ff", "#bdd7e7", "#6baed6", "#3182bd"],
        ],
        dtype=object,
    )
    prob_mass_for_credible_region = np.atleast_1d(prob_mass_for_credible_region)
    n_color = len(prob_mass_for_credible_region)
    colors = colors[n_color - 1]

    prob_mass_for_credible_region = np.sort(prob_mass_for_credible_region)[::-1]
    if prob_mass_for_credible_region[0] > prob_mass_for_plotting_range:
        prob_mass_for_plotting_range = prob_mass_for_credible_region[0]
        logging.info(
            f"The `prob_mass_for_plotting_range` parameter was set "
            f"to {prob_mass_for_plotting_range} to allow the plotting of the "
            f"specified `prob_mass_for_credible_region`."
        )

    n_dimension = sample.shape[1]
    if n_dimension > n_dimension_limit:
        n_dimension = n_dimension_limit
        warnings.warn(
            f"Too many dimensions. Only the first {n_dimension} dimensions "
            f"(variables) are visualized. If you would like to plot more "
            f"dimensions increase the value of the `n_dimension_limit` "
            f"parameter.",
            UserWarning,
        )

    def dim_label_fun(dim):
        if dim_labels is None:
            label = f"$θ_{{{dim+1}}}$"
        else:
            label = dim_labels[dim]
        return label

    # ----------------------------------------------------------------------------------
    # GET SUPPORT RANGE
    # ----------------------------------------------------------------------------------
    prob_mass_out = 1 - prob_mass_for_plotting_range
    quantile_prob = np.array(
        [prob_mass_out / 2, prob_mass_for_plotting_range + prob_mass_out / 2]
    )
    if plotting_range is None:
        plotting_range = weighted_sample_quantile(
            sample=sample, quantile_prob=quantile_prob, weight_vec=weight_vec, axis=0
        )

    # ----------------------------------------------------------------------------------
    # PLOT
    # ----------------------------------------------------------------------------------
    if n_dimension == 1:
        # we need to leave more space for the legend
        figsize = (n_dimension + 3 + 3, n_dimension + 3)
    else:
        figsize = (n_dimension + 3, n_dimension + 3)

    fig, axes = plt.subplots(
        n_dimension,
        n_dimension,
        figsize=figsize,
        squeeze=False,
    )
    z_max = []

    for x_i in range(n_dimension):
        for y_i in range(n_dimension):
            # turn off the upper triangle
            if x_i < y_i:
                axes[x_i, y_i].axis("off")

            # diagonal
            elif x_i == y_i:
                y_labelpad = 15
                xx = sample[:, x_i]
                median_x = weighted_sample_quantile(
                    sample=xx, quantile_prob=0.5, weight_vec=weight_vec
                )

                _, density, x_mesh = kde_1d(
                    sample_vec=xx, weight_vec=weight_vec, n_x_vec=int(kde_1d_n_x_vec)
                )
                density_at_median = np.interp(median_x, x_mesh, density)
                axes[x_i, x_i].plot(x_mesh, density, color=color_line_1d)

                # Compute the credible region
                try:
                    cr_boundaries, p_star_list = get_credible_region_1d(
                        x_vec=x_mesh,
                        density_vec=density,
                        prob_mass=prob_mass_for_credible_region,
                    )

                    for pp, p_star in enumerate(p_star_list):
                        cr_index = prob_mass_for_credible_region[pp]
                        for cr_boundary in cr_boundaries[cr_index]:
                            idx_region = np.logical_and(
                                x_mesh > cr_boundary[0], x_mesh < cr_boundary[1]
                            )
                            x_fill = x_mesh[idx_region].ravel()
                            density_fill = density[idx_region].ravel()
                            axes[x_i, x_i].fill_between(
                                x=x_fill, y1=0, y2=density_fill, color=colors[pp]
                            )

                        # not needed currently but later it might come handy
                        axes[x_i, x_i].vlines(
                            x=cr_boundaries[cr_index],
                            ymin=0,
                            ymax=p_star,
                            color=colors[pp],
                        )
                except (ValueError, ConvergenceError) as error:
                    warnings.warn(
                        f"Computing the 1D credible region for parameter "
                        f"{x_i} has failed with error: {error}. \n The "
                        f"credible region is not plotted."
                    )

                if ground_truth is None:
                    pass
                else:
                    axes[x_i, x_i].axvline(
                        x=ground_truth[x_i],
                        ymin=0,
                        ymax=1,
                        color="red",
                        linestyle="dashed",
                        linewidth=1,
                    )
                axes[x_i, x_i].vlines(
                    x=median_x,
                    ymin=0,
                    ymax=density_at_median,
                    color=color_line_1d,
                    linestyle="dashed",
                )
                axes[x_i, x_i].set_xlim(plotting_range[:, x_i])
                axes[x_i, x_i].set_ylim(0)
                axes[x_i, x_i].yaxis.tick_right()
                axes[x_i, x_i].yaxis.set_label_position("right")
                axes[x_i, x_i].set_ylabel("$p$", rotation=0, labelpad=y_labelpad)

                z_max.append(np.max(density))

                if x_i == n_dimension - 1:
                    axes[x_i, x_i].set_xlabel(dim_label_fun(x_i))
                else:
                    axes[x_i, x_i].tick_params(labelbottom=False)
                    axes[x_i, y_i].xaxis.offsetText.set_visible(False)

            # off diagonal
            else:
                y_labelpad = 10

                xx = sample[:, y_i]
                yy = sample[:, x_i]
                data = np.hstack((xx.reshape(-1, 1), yy.reshape(-1, 1)))
                bandwidth, density, x_mesh, y_mesh = kde_2d(
                    sample_mx=data,
                    weight_vec=weight_vec,
                    n_row_mx=int(kde_2d_n_row_mx),
                )

                # Compute the credible region
                try:
                    cr_boundaries, p_star_list = get_credible_region_2d(
                        x_mx=x_mesh,
                        y_mx=y_mesh,
                        density_mx=density,
                        prob_mass=prob_mass_for_credible_region,
                    )
                    for ll, level in enumerate(cr_boundaries):
                        for island in cr_boundaries[level]:
                            axes[x_i, y_i].plot(
                                island[:, 0], island[:, 1], color=colors[ll]
                            )
                except (ValueError, ConvergenceError) as error:
                    warnings.warn(
                        f"Computing the 2D credible region for parameter pairs "
                        f"({x_i}, {y_i}) has failed with error: {error}. \n The "
                        f"credible region is not plotted."
                    )

                if ground_truth is None:
                    pass
                else:
                    axes[x_i, y_i].vlines(
                        x=ground_truth[y_i],
                        ymin=plotting_range[0, x_i],
                        ymax=plotting_range[1, x_i],
                        color="red",
                        linestyle="dashed",
                        linewidth=1,
                    )
                    axes[x_i, y_i].hlines(
                        y=ground_truth[x_i],
                        xmin=plotting_range[0, y_i],
                        xmax=plotting_range[1, y_i],
                        color="red",
                        linestyle="dashed",
                        linewidth=1,
                    )
                axes[x_i, y_i].imshow(
                    density,
                    cmap="Greys",
                    origin="lower",
                    extent=[x_mesh.min(), x_mesh.max(), y_mesh.min(), y_mesh.max()],
                )

                axes[x_i, y_i].axis("auto")
                axes[x_i, y_i].set_xlim(plotting_range[:, y_i])
                axes[x_i, y_i].set_ylim(plotting_range[:, x_i])

                # left bottom corner
                if x_i == n_dimension - 1 and y_i == 0:
                    axes[x_i, y_i].set_xlabel(dim_label_fun(y_i))
                    axes[x_i, y_i].set_ylabel(
                        dim_label_fun(x_i), rotation=90, labelpad=y_labelpad
                    )

                # bottom edge x label
                elif x_i == n_dimension - 1:
                    if y_i != 0:
                        axes[x_i, y_i].tick_params(labelleft=False)
                        axes[x_i, y_i].yaxis.offsetText.set_visible(False)
                    axes[x_i, y_i].set_xlabel(dim_label_fun(y_i))

                # left edge x label
                elif y_i == 0:
                    # axes[x_i, y_i].set_xticklabels([])
                    axes[x_i, y_i].tick_params(labelbottom=False)
                    axes[x_i, y_i].xaxis.offsetText.set_visible(False)
                    axes[x_i, y_i].set_ylabel(
                        dim_label_fun(x_i), rotation=90, labelpad=y_labelpad
                    )

                # rest
                else:
                    axes[x_i, y_i].tick_params(labelleft=False)
                    axes[x_i, y_i].tick_params(labelbottom=False)
                    axes[x_i, y_i].yaxis.offsetText.set_visible(False)
                    axes[x_i, y_i].xaxis.offsetText.set_visible(False)

            # Format the axes - for all plots
            axes[x_i, y_i].tick_params(
                axis="both",
                direction="in",
                which="both",
                left=True,
                top=True,
                right=True,
                labelsize=6,
                colors="#4D4D4D",
            )
            axes[x_i, y_i].xaxis.offsetText.set_fontsize(6)
            axes[x_i, y_i].yaxis.offsetText.set_fontsize(6)
            axes[x_i, y_i].ticklabel_format(axis="both", scilimits=(-4, 4))
            axes[x_i, y_i].xaxis.label.set_color("#4D4D4D")
            axes[x_i, y_i].yaxis.label.set_color("#4D4D4D")

    # ----------------------------------------------------------------------------------
    # SET THE RANGES AND COLORS
    # ----------------------------------------------------------------------------------
    fig.suptitle(title)
    if display_legend:
        legend_elements = []
        if prob_mass_for_credible_region != np.array([0]):
            for ii, col in enumerate(colors):
                element = Line2D(
                    [0],
                    [0],
                    color=col,
                    linewidth=2,
                    label=f"$P$={prob_mass_for_credible_region[ii]:.2f} "
                    f"(hd) credible region",
                )
                legend_elements.append(element)

        element_median = Line2D(
            [0],
            [0],
            linestyle="dashed",
            color=color_line_1d,
            linewidth=2,
            label="Median",
        )
        legend_elements.append(element_median)
        if ground_truth is not None:
            element_ground_truth = Line2D(
                [0],
                [0],
                linestyle="dashed",
                linewidth=1,
                color="red",
                label="Ground truth",
            )
            legend_elements.append(element_ground_truth)

        ax_bounds = np.array([list(ax.bbox._bbox.bounds) for ax in fig.axes])
        top_margin = 1 - (ax_bounds[:, 0] + ax_bounds[:, 2]).max()
        right_margin = 1 - (ax_bounds[:, 1] + ax_bounds[:, 3]).max()
        # right_margin = ax_bounds[:, 0].min()
        # top_margin = ax_bounds[:, 1].min()
        bbox_to_anchor = (1.0 - right_margin / 2, 1.0 - top_margin)
        loc = "upper right"

        if n_dimension == 1:
            fig.subplots_adjust(right=0.6)

        fig.legend(
            handles=legend_elements,
            loc=loc,
            bbox_to_anchor=bbox_to_anchor,
            fontsize=8,
        )

    if save_fig is True:
        fname = kwargs.get("fname", "fig")
        plt.savefig(fname=fname, **kwargs)

    return (fig, axes), plotting_range
