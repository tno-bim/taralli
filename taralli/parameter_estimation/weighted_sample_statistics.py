"""Functions to compute statistics of weighted samples."""

from typing import Union

import numpy as np


def weighted_sample_mean(
    sample: Union[np.ndarray, list], weight_vec: Union[np.ndarray, list] = None
) -> np.ndarray:
    """
    Mean of a weighted sample.

    Args:
        sample: 2D matrix (NxK), columns: sample point coordinates (K), rows: sample
            points (N).
        weight_vec: a weight for each sample point (N elements).

    Returns:
        mean: mean of the weighted sample (K elements).
    """
    mean = np.average(sample, weights=weight_vec, axis=0)
    return np.atleast_1d(mean)


def weighted_sample_covariance(
    sample: Union[np.ndarray, list], weight_vec: Union[np.ndarray, list] = None
) -> np.ndarray:
    """
    Covariance of a weighted sample.

    Args:
        sample: 2D matrix (NxK), columns: sample point coordinates (K), rows: sample
            points (N).
        weight_vec: a weight for each sample point (N elements).

    Returns:
        cov: covariance matrix of the weighted sample (KxK).

    """
    cov = np.cov(sample.T, aweights=weight_vec)
    return np.atleast_2d(cov)


def weighted_sample_quantile(
    sample: Union[np.ndarray, list],
    quantile_prob: Union[np.ndarray, list, float, int],
    weight_vec: Union[np.ndarray, list] = None,
    axis: Union[int, None] = 0,
) -> np.ndarray:
    """
    Quantile(s) of a weighted sample.

    `quantile_prob = P(X < quantile_val)`

    `sample` is drawn from `X`, `P(X<x)` is approximated with the empirical
    cumulative distribution of `sample`.

    Args:
        sample: sample points (NxK).
        quantile_prob: quantile or sequence of quantile non-exceedance probabilities;
            they must be between 0 and 1 inclusive (P elements).
        weight_vec: a weight for each sample point (N elements).
        axis: axis along which the quantile values are computed. The default is to
            compute the quantile(s) along the rows of `sample`. If `None` then the
            quantile values are computer from a flattened version of `sample`.

    Returns:
        quantile_val: quantiles of the weighted sample (PxK).

    Notes:
        The quantile implementation uses the (k-1)/(n-1) plotting position
        (https://en.wikipedia.org/wiki/Q%E2%80%93Q_plot#Heuristics).
    """
    sample = np.atleast_1d(sample)
    quantile_prob = np.atleast_1d(quantile_prob)
    n_quantile_prob = len(quantile_prob)

    if axis is None or len(sample.shape) == 1:
        sample = sample.reshape((-1, 1))

    if np.any(quantile_prob < 0.0) or np.any(quantile_prob > 1.0):
        raise ValueError("Quantiles must be between 0 and 1.")

    if weight_vec is None:
        quantile_val = np.quantile(sample, q=list(quantile_prob), axis=0)
    else:
        weight_vec = np.atleast_1d(weight_vec)
        if len(sample) != len(weight_vec):
            raise ValueError("Dimension mismatch: len(weight_vec) != len(sample)")

        # get rid of points with zero weight
        idx_zero = weight_vec != 0.0
        sample = sample[idx_zero, :]
        weight_vec = weight_vec[idx_zero]

        n_column = sample.shape[1]
        quantile_val = np.empty((n_quantile_prob, n_column))
        for ii in range(n_column):
            # CDF calculation
            sample_column = sample[:, ii]
            idx = np.argsort(sample_column)
            sw = weight_vec[idx]
            cdf_unscaled = np.cumsum(sw)
            cdf = (cdf_unscaled - cdf_unscaled[0]) / (
                cdf_unscaled[-1] - cdf_unscaled[0]
            )
            quantile_val[:, ii] = np.interp(
                x=quantile_prob, xp=cdf, fp=sample_column[idx]
            )
        quantile_val = quantile_val.reshape(-1, n_column)
    return quantile_val


def weighted_sample_median_absolute_deviation(
    sample: Union[np.ndarray, list],
    weight_vec: Union[np.ndarray, list] = None,
    scale: float = 1.482602218505602,
) -> np.ndarray:
    x_med = weighted_sample_quantile(
        sample=sample, quantile_prob=0.5, weight_vec=weight_vec
    )
    y = np.atleast_1d(np.abs(sample - x_med))
    if y.shape[0] == 1:
        y = y.T
    x_mad = (
        weighted_sample_quantile(sample=y, quantile_prob=0.5, weight_vec=weight_vec)
        * scale
    )
    return np.atleast_1d(x_mad)
