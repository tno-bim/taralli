"""
TODO: - Document get_credible_region_1d fun
      - Document get_credible_region_2d fun
      - Improve extreme optimization
"""
from typing import Iterable, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
from scipy import integrate
from scipy import optimize

from taralli.utilities.custom_error import ConvergenceError


def trapz2(
    x_vec: Union[np.ndarray, Iterable],
    y_vec: Union[np.ndarray, Iterable],
    z_mx: np.ndarray,
) -> float:
    """
    Two dimensional trapezoidal numerical integration.

    A naive but simple implementation.

    Args:
        x_vec: vector of grid points in one dimension, with length n_x.
        y_vec: vector of grid points in other dimension, with length n_y.
        z_mx: matrix of function values evaluated at meshgrid(y, x) grid-points,
            shape = (n_y, n_x).

    Returns:
        q: value of integral

    Examples:
        >>> import numpy as np
        >>>
        >>> step = 0.1
        >>> x_vec = np.arange(-2, 2 + step, step=step)
        >>> y_vec = np.arange(-1, 1 + step, step=step)
        >>> x_mx, y_mx = np.meshgrid(x_vec, y_vec)
        >>> z_mx = x_mx**2 + y_mx**2
        >>> q = trapz2(x_vec, y_vec, z_mx)
    """
    q_x = np.array([np.trapz(z_col_ii, y_vec) for z_col_ii in z_mx.T])
    q = np.trapz(q_x, x_vec)
    return q


def add_zeros_edges_2d(x_mx: np.ndarray, y_mx: np.ndarray, z_mx: np.ndarray):
    """Add edges to `x_mx`, `y_mx`, `density_mx` to ensure the "constructability" of
    the credible regions.

    Args:
        x_mx: grid coordinates along the x-axis.
        y_mx: grid coordinates along the y-axis.
        z_mx: grid coordinates along the z-axis.

    Returns:
        x_mx, y_mx, density_mx: grid coordinates with added edges with density_mx=0.
    """
    x_vec = x_mx[0]
    y_vec = y_mx[:, 0]
    n_x = len(x_vec)
    n_y = len(y_vec)
    delta_x = np.max(x_vec) - np.min(x_vec)
    delta_y = np.max(y_vec) - np.min(y_vec)
    x_vec = np.hstack(
        (np.min(x_vec) - 1e-12 * delta_x, x_vec, np.max(x_vec) + 1e-12 * delta_x)
    )
    y_vec = np.hstack(
        (np.min(y_vec) - 1e-12 * delta_y, y_vec, np.max(y_vec) + 1e-12 * delta_y)
    )
    x_mx, y_mx = np.meshgrid(x_vec, y_vec)
    z_mx_0 = np.zeros(x_mx.shape)
    z_mx_0[1 : n_y + 1, 1 : n_x + 1] = z_mx
    z_mx = z_mx_0

    return x_mx, y_mx, z_mx


def get_contour_1d(x_mx, z_mx, p_star) -> np.ndarray:
    """Get coordinates for a 1d contour plot.

    TODO:
     * harmonize it with the 2d version.

    Args:
        x_mx: grid coordinates along the x-axis.
        z_mx: grid coordinates along the z-axis.
        p_star: z value for which the contour plot is obtained.

    Returns:
        p: array of coordinates of the credible region.
    """
    y_mx = np.array([np.ones(x_mx.shape[1]), np.ones(x_mx.shape[1]) * 2])

    plt.ioff()
    fig_tmp, ax_tmp = plt.subplots()
    cs = ax_tmp.contour(x_mx, y_mx, z_mx, levels=p_star)

    p = []
    for collection in cs.collections:
        for path in collection.get_paths():
            p.append(path.vertices[0, 0])

    plt.close(fig_tmp)
    plt.ion()
    return np.array(p).reshape(-1, 2)


def get_contour_2d(x_mx, y_mx, z_mx, p_star):
    """Get coordinates for a 2d contour plot.

    Args:
        x_mx: grid coordinates along the x-axis.
        y_mx: grid coordinates along the y-axis.
        z_mx: grid coordinates along the z-axis.
        p_star: z value for which the contour plot is obtained.

    Returns:
        p: array of coordinates of the credible region.
    """
    plt.ioff()
    fig_tmp, ax_tmp = plt.subplots()
    cs = ax_tmp.contour(x_mx, y_mx, z_mx, levels=p_star)

    p = []
    for collection in cs.collections:
        for path in collection.get_paths():
            p.append(path.vertices)
    plt.close(fig_tmp)
    plt.ion()
    return p


def get_credible_region_1d(
    x_vec: np.ndarray,
    density_vec: np.ndarray,
    prob_mass: Union[float, np.ndarray] = 0.90,
) -> Tuple[dict, list]:
    """Credible region for a 1D density function given with arrays (`x_vec`,
    `density_vec`).

    Args:
        x_vec:
        density_vec:
        prob_mass: probability mass inside the credible region.

    Returns:
            x_boundaries: [
                        [interval_1_start, interval_1_end],
                        [interval_2_start, interval_2_end]
                        ...
                       ]
            p_star: likelihood corresponding to f(interval_i_start) = f(interval_j_end)
    """

    prob_mass = np.atleast_1d(prob_mass)

    # add zeros at the edges
    delta_support = np.max(x_vec) - np.min(x_vec)
    x_vec = np.hstack(
        (
            np.min(x_vec) - 1e-12 * delta_support,
            x_vec,
            np.max(x_vec) + 1e-12 * delta_support,
        )
    )
    density_vec = np.hstack((0, density_vec, 0))

    x_mx = np.array([x_vec, x_vec])
    z_mx = np.tile(density_vec, (2, 1))

    p_star_list = []
    x_bd_dict = dict.fromkeys(prob_mass)

    for mm in prob_mass:
        opt_res = optimize.brentq(
            f=prob_mass_diff_1d,
            a=0,
            b=np.max(density_vec),
            args=(x_vec, density_vec, mm),
            full_output=True,
        )
        p_star = np.array([opt_res[0]])
        conv = opt_res[1]
        if conv.converged is False:
            raise ConvergenceError(
                "The root finding algorithm failed to converge in finding `p_star`."
            )
        p_star_list.append(p_star)

        x_boundaries = get_contour_1d(x_mx, z_mx, p_star)
        x_bd_dict[mm] = x_boundaries

    return x_bd_dict, p_star_list


def get_credible_region_2d(
    x_mx: np.ndarray,
    y_mx: np.ndarray,
    density_mx: np.ndarray,
    prob_mass: Union[float, np.ndarray] = 0.90,
) -> Tuple[dict, list]:
    """Credible region for a 2D density function given with arrays (`x_mx`, `y_mx`,
    `density_mx`).

    Args:
        x_mx: grid coordinates along the x-axis.
        y_mx: grid coordinates along the y-axis.
        density_mx: grid coordinates along the z-axis.
        prob_mass: probability mass inside the credible region.

    Returns:

    """

    prob_mass = np.atleast_1d(prob_mass)
    # add zeros at the edges
    x_mx, y_mx, density_mx = add_zeros_edges_2d(x_mx, y_mx, density_mx)

    x_vec = x_mx[0]
    y_vec = y_mx[:, 0]

    p_star_list = []
    x_bd_dict = dict.fromkeys(prob_mass)

    for mm in prob_mass:
        opt_res = optimize.brentq(
            f=prob_mass_diff_2d,
            a=0,
            b=np.max(density_mx),
            args=(x_vec, y_vec, density_mx, mm),
            full_output=True,
        )
        p_star = np.array([opt_res[0]])
        conv = opt_res[1]
        if conv.converged is False:
            raise ValueError("The optimization to find p_star was not successful")

        p_star_list.append(p_star)

        xy_boundaries = get_contour_2d(x_mx, y_mx, density_mx, p_star)
        x_bd_dict[mm] = xy_boundaries

    return x_bd_dict, p_star_list


def prob_mass_diff_1d(p, x_vec, density_vec, prob_mass):
    """Define the function to find the root of: `fun(p) = A(p) - prob_mass`.

    Args:
        p: z-value defining the credible region.
        x_vec: x_vec.
        density_vec:
        prob_mass: target area of the credible region.

    Returns:
        area_diff: difference between the area of the credible region defined by `p`
            and the target `prob_mass`.
    """
    ff = np.copy(density_vec)
    ff[density_vec <= p] = 0
    mass_diff = integrate.trapz(ff, x_vec) - prob_mass
    return mass_diff


def prob_mass_diff_2d(p, x_vec, y_vec, density_mx, prob_mass):
    """Define the function to find the root of: `fun(p) = A(p) - prob_mass`.

    Args:
        p: z-value defining the credible region.
        x_vec: vector of grid points in one dimension, with length n_x
        y_vec: vector of grid points in other dimension, with length n_y
        density_mx: matrix of function values evaluated at meshgrid(y, x)
            grid-points, shape = (n_y, n_x).
        prob_mass: target area of the credible region.

    Returns:
        area_diff: difference between the area of the credible region defined by
            `p` and the target prob_mass.
    """
    ff = np.copy(density_mx)
    ff[density_mx < p] = 0
    mass_diff = trapz2(x_vec, y_vec, ff) - prob_mass
    return mass_diff
