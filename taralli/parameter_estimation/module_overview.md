# Parameter Estimation module

## Function

## Module overview

```mermaid
classDiagram
    StatModel <.. EmceeParameterEstimator
    StatModel <.. DynestyParameterEstimator
    StatModel <.. NestleParameterEstimator
    StatModel: n_dim
    StatModel: log_likelihood
    StatModel: log_prior/prior_transform
    StatModel: estimate_params()
    StatModel: plot_marginal_matrix()
    class EmceeParameterEstimator{
        estimate_params()
    }
    class DynestyParameterEstimator{
        estimate_params()
    }
    class NestleParameterEstimator{
        estimate_params()
    }

```
