from typing import Union


def pretty_time_delta(seconds: Union[float, int]) -> str:
    """
    Convert seconds into human friendly time string.

    Source:
    https://gist.github.com/thatalextaylor/7408395#file-1-python-pretty-time-delta-py

    Args:
        seconds:

    Returns:
        Human friendly time difference in string format.
    """
    sign_string = "-" if seconds < 0 else ""
    seconds = abs(int(seconds))
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return "%s%dd%dh%dm" % (sign_string, days, hours, minutes)
    elif hours > 0:
        return "%s%dh%dm%ds" % (sign_string, hours, minutes, seconds)
    elif minutes > 0:
        return "%s%dm%ds" % (sign_string, minutes, seconds)
    else:
        return "%s%ds" % (sign_string, seconds)
