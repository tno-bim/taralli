class Error(Exception):
    """Base class for exceptions."""

    pass


class ConvergenceError(Error):
    """Raised when an algorithm does not converge"""

    pass
