import logging.config
import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Custom logging config
LOGGING_CONFIG = None
LOG_FILE = f"{BASE_DIR}/taralli.log"

taralli_logging_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "format": "[taralli][%(asctime)s] %(levelname)s: %(message)s",
            "datefmt": "%Y-%b-%d %H:%M:%S",
        },
        "detail": {
            "format": "[taralli][%(asctime)s] %(levelname)s: %(message)s [log "
            "made in %(pathname)s:%(lineno)d]"
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "stream": sys.stdout,
            "formatter": "default",
        },
        "file": {
            "class": "logging.FileHandler",
            "level": "INFO",
            "formatter": "detail",
            "filename": LOG_FILE,
        },
    },
    "root": {"level": "INFO", "handlers": ["console", "file"], "propagate": True},
}

logging.config.dictConfig(taralli_logging_config)
