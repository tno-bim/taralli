"""
 Utility functions for the log-likelihood builder module.
"""
from typing import Callable, Union

import numpy as np
from scipy.spatial.distance import pdist, squareform


def correlation_matrix(
    x_mx,
    correlation_func: Callable[[np.ndarray], np.ndarray],
    distance_metric: str = "euclidean",
) -> np.ndarray:
    """
    Statistical correlation matrix between measurement points based on their distance.

    Args:
        x_mx: the coordinates of the measurement points between which the correlation
            matrix is calculated, (N,K).
        correlation_func: a function that calculates the statistical correlation
            between measurements made at two points that are `d` units distant from
            each other, r_major^m -> r_major^m.
        distance_metric: The distance metric to use. The distance function is the
            same as the `metric` input argument of `scipy.spatial.distance.pdist`.

    Returns:
        rho_mx: correlation matrix.
    """

    d = squareform(pdist(x_mx, metric=distance_metric))
    rho_mx = correlation_func(d)

    return rho_mx


def correlation_function(
    d: Union[np.ndarray, list, float, int],
    correlation_length: Union[float, int],
    function_type: str = "exponential",
    exponent: Union[float, int] = 2,
) -> np.ndarray:
    """
    Statistical correlation between measurements made at two points that are at `d`
    units distance from each other.

    Args:
        d: distance(s) between points.
        correlation_length: `1/correlation_length` controls the strength of
            correlation between two points. `correlation_length = 0` => complete
            independence for all point pairs (`rho=0`). `correlation_length = Inf` =>
            full dependence for all point pairs (`rho=1`).
        function_type: name of the correlation function.
        exponent: exponent in the type="cauchy" function. The larger the exponent the
            less correlated two points are.

    Returns:
        rho: correlation coefficient for each element of `d`.
    """
    function_type = function_type.lower()

    if correlation_length < 0:
        raise ValueError("correlation_length must be a non-negative number.")
    if np.any(d < 0):
        raise ValueError("All elements of d must be non-negative numbers.")

    if correlation_length == 0:
        idx = d == 0
        rho = np.zeros(d.shape)
        rho[idx] = 1
    elif correlation_length == np.inf:
        rho = np.ones(d.shape)
    else:
        if function_type == "exponential":
            rho = np.exp(-d / correlation_length)
        elif function_type == "cauchy":
            rho = (1 + (d / correlation_length) ** 2) ** -exponent
        elif function_type == "gaussian":
            rho = np.exp(-((d / correlation_length) ** 2))
        else:
            raise ValueError(
                "Unknown function_type. It must be one of these: 'exponential', "
                "'cauchy', 'gaussian'."
            )
    return rho


def grow_mx(seed_mx: np.ndarray, growth_scale: int) -> np.ndarray:
    """
    Grow a 2D array while keeping its original "look".

    Args:
        seed_mx: 2D-array to be grown
        growth_scale:

    Returns:
        growth_mx
    """
    grown_list = [
        [np.ones((growth_scale, growth_scale)) * elem for elem in row]
        for row in seed_mx
    ]
    grown_mx = np.block(grown_list)
    return grown_mx
