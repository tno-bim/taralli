"""This module helps to build likelihood functions.
* complex covariance matrices;
* common log-likelihood function;
* efficient log-likelihood evaluation."""
