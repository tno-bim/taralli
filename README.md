# taralli

![tarallo](docs/_static/taralli.gif)

A parameter estimation tool for statistical problems with a computationally demanding likelihood function (that is comparably expensive to evaluate as to simulate from it).

[![documentation](https://img.shields.io/badge/docs-master-blue)](https://tno-bim.gitlab.io/taralli/)
[![pipeline](https://gitlab.com/tno-bim/taralli/badges/master/pipeline.svg)](https://en.wikipedia.org/wiki/Pipeline_(computing))
[![coverage](https://gitlab.com/tno-bim/taralli/badges/master/coverage.svg)](https://en.wikipedia.org/wiki/Code_coverage)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Package overview

```mermaid
graph RL
    %% linkStyle default interpolate basis

    subgraph parameter_estimation
        vis_param_est(Visualization) --> param_est(Parameter estimation)
    end

    subgraph likelihood_builder
        vis_like_fun(Visualization) --> like_fun(Likelihood utilities)
        meas(Measurement points) --> like_fun
    end

    subgraph surrogating
        vis_surrogate(Visualization) --> surrogate(Surrogating)
    end

    subgraph reliability_analysis
        vis_reli(Visualization) --> reli(Reliability)
    end

    param_est -.- user((User))
    like_fun -.- user
    surrogate -.- user
    reli -.- user

    click param_est "https://gitlab.com/tno-bim/taralli/-/tree/master/taralli%2Fparameter_estimation"
    click like_fun "https://gitlab.com/tno-bim/taralli/-/tree/master/taralli%2Floglikelihood_builder"
    click surrogate "https://gitlab.com/tno-bim/taralli/-/tree/master/taralli%2surrogating"
    click reli "https://gitlab.com/tno-bim/taralli/-/tree/master/taralli%2Freliability_analysis"

    style parameter_estimation fill:#fff3eb, stroke:#ff6600
    style param_est fill:#fff, stroke:#ff6600
    style vis_param_est fill:#fff, stroke:#ff6600, stroke-dasharray: 5, 5

    style likelihood_builder fill:#fffbeb, stroke:#ffcc00
    style like_fun fill:#fff, stroke:#ffcc00
    style vis_like_fun fill:#fff, stroke:#ffcc00, stroke-dasharray: 5, 5
    style meas fill:#fff, stroke:#ffcc00, stroke-dasharray: 5, 5

    style surrogating fill:#e0ffe0, stroke:#00cc00
    style surrogate fill:#fff, stroke:#00cc00
    style vis_surrogate fill:#fff, stroke:#00cc00, stroke-dasharray: 5, 5

    style reliability_analysis fill:#ebf3ff, stroke:#0066ff
    style reli fill:#fff, stroke:#0066ff
    style vis_reli fill:#fff, stroke:#0066ff, stroke-dasharray: 5, 5
```

## Documentation

The latest documentation for each branch:
- [master](https://gitlab.com/tno-bim/taralli)
- [all other branches](https://gitlab.com/tno-bim/taralli/-/environments)


----
## On using the repository

* Running the code: All code within this repository is expected to be run with the
  working directory set as the root directory of the repository: `\taralli\`.
* Developed and tested under Python 3.6, 3.7, 3.8 (expected to work with python 3).
* Developed and tested under Windows 10.
* In order to keep the repository cleaner, the figures/plots are not kept under
  version control, you should regenerate them locally.


## Contributing

We follow/use
* [PEP8](https://www.python.org/dev/peps/pep-0008/)
* [black](https://github.com/psf/black)
* [type hinting](https://docs.python.org/3/library/typing.html)
* [Google docstring](https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings)

If the provided base code does not follow these recommendations then please fix it.
