import os

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

# Package requirements
user_requires = [
    "numpy<2",
    "scipy<2",
    "nestle<1",
    "emcee<4",
    "dynesty<2",
    "toolz<1",
    "matplotlib<4",
    "tabulate<1",
    "tqdm<5",
]
doc_requires = [
    "six<2",
    "sphinx>=3.0.1,<5",
    "sphinx-copybutton<1",
    "sphinxcontrib-mermaid!=0.6.1,<1",
    "sphinx-inline-tabs",
    "sphinxcontrib-bibtex<3",
    "myst-parser<1",
    "furo",
]
test_requires = [
    "pytest",
    "coverage",
]
packaging_requires = [
    "twine",
]
format_requires = [
    "pre-commit",
    "black",
    "flake8",
    "flake8-tidy-imports",
    "flake8-import-order",
]

# In order to reduce the time to build the documentation
# https://github.com/readthedocs/readthedocs.org/issues/5512#issuecomment-475073310
on_ci_doc = os.environ.get("CI_DOC") is not None
if on_ci_doc:
    install_requires = doc_requires
else:
    install_requires = user_requires

setuptools.setup(
    name="taralli",
    version="0.8.4",
    author="Arpad Rozsas, Giulia Martini",
    author_email="arpad.rozsas@tno.nl",
    description="A parameter estimation tool for statistical problems with a "
    "computationally demanding likelihood function (that is comparably "
    "expensive to evaluate as to simulate from it).",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tno-bim/taralli/",
    packages=setuptools.find_packages(),
    # packages that are needed for the users
    install_requires=install_requires,
    # additional packages for development (additional to `install_requires`)
    extras_require={
        "development": [
            *doc_requires,
            *test_requires,
            *packaging_requires,
            *format_requires,
        ],
        "testing": test_requires,
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: TBD",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering",
        "Intended Audience :: Science/Research",
    ],
    python_requires=">=3.6",
)
