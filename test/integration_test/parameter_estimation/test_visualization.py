"""
Note that these test are very weak, i.e. we cannot automatically check if the plots
look good but at least we can see if they raise an error or not.

This test suite is also used for manual(human) checks.
"""

import matplotlib.pyplot as plt
import numpy as np
import pytest
from scipy import stats

from taralli.parameter_estimation.visualization import visualize_marginal_matrix

plot_fig = False


# ======================================================================================
# NORMAL behavior (no assert statements)
# ======================================================================================
def test_multivariate_ndim():
    np.random.seed(42)
    n_dim = 4
    n_sample = 5000
    sample_1 = (
        stats.multivariate_normal.rvs(mean=0 * np.ones(n_dim), size=int(n_sample / 2))
        * 2e0
    )
    sample_2 = (
        stats.multivariate_normal.rvs(mean=8 * np.ones(n_dim), size=n_sample) * 1e0
    )
    sample_3 = (
        stats.multivariate_normal.rvs(
            mean=3 * (-1) ** np.arange(n_dim), size=int(n_sample / 2)
        )
        * 1e0
    )
    sample = np.vstack(
        (
            sample_1.reshape((-1, n_dim)),
            sample_2.reshape((-1, n_dim)),
            sample_3.reshape((-1, n_dim)),
        )
    )
    all_dim_labels = [None, [f"custom_label_{ii}" for ii in range(n_dim)]]
    titles = ["", "Custom title"]
    mass = 0.7
    for dim_labels, title in zip(all_dim_labels, titles):
        visualize_marginal_matrix(
            sample=sample,
            prob_mass_for_credible_region=mass,
            dim_labels=dim_labels,
            title=title,
        )

    if plot_fig:
        plt.show()


def test_multivariate_ndim_with_weights():
    np.random.seed(42)
    n_dim = 2
    n_sample = 5000
    sample = stats.multivariate_normal.rvs(
        mean=0 * np.ones(n_dim), size=n_sample
    ).reshape((-1, n_dim))
    sample = sample[sample[:, 0].argsort()]

    weights = np.ones(n_sample)
    weights[1 : int(n_sample / 3)] = 0.1

    mass = 0.7
    visualize_marginal_matrix(
        sample=sample, prob_mass_for_credible_region=mass, weight_vec=weights
    )

    if plot_fig:
        plt.show()


def test_3d_with_different_densities():
    """
    Markedly different heights for the density marginals.
    """
    np.random.seed(42)
    n_sample = 5000
    sample_x1 = stats.norm.rvs(loc=0, scale=1, size=n_sample).reshape((-1, 1))
    sample_x2 = stats.norm.rvs(loc=-3, scale=3, size=n_sample).reshape((-1, 1))
    sample_x3 = stats.norm.rvs(loc=3, scale=5, size=n_sample).reshape((-1, 1))

    sample = np.hstack(
        (
            sample_x1,
            sample_x2,
            sample_x3,
        )
    )

    mass = 0.7
    visualize_marginal_matrix(sample=sample, prob_mass_for_credible_region=mass)

    if plot_fig:
        plt.show()


def test_legend_placement():
    np.random.seed(42)
    n_dims = [1, 2, 3]
    n_max_dim = max(n_dims)
    n_sample = 100
    sample = stats.multivariate_normal.rvs(mean=np.zeros(n_max_dim), size=n_sample)
    mass = 0.7

    for n_dim in n_dims:
        visualize_marginal_matrix(
            sample=sample[:, 0:n_dim],
            prob_mass_for_credible_region=mass,
            ground_truth=np.zeros(n_max_dim),
            title=f"{n_dim} parameter(s) of interest",
        )


def test_too_large_dim_to_plot():
    n_sample = 1000
    n_dim = 3
    n_dimension_limit = 2
    sample = stats.multivariate_normal.rvs(mean=np.ones(n_dim), size=n_sample)

    with pytest.warns(UserWarning, match="Too many dimensions"):
        visualize_marginal_matrix(sample=sample, n_dimension_limit=n_dimension_limit)


# ======================================================================================
# CHECK errors
# ======================================================================================
# Input errors TODO: to be added

# Fail to compute the credible region (root finding error)


def test_credible_region_fail():
    np.random.seed(42)
    n_dim = 2
    n_sample = 20
    sample = stats.multivariate_normal.rvs(mean=np.zeros(n_dim), size=n_sample)

    with pytest.warns(UserWarning, match="The credible region is not plotted"):
        # 1D credible region will fail
        visualize_marginal_matrix(sample=sample, prob_mass_for_credible_region=0.99)
