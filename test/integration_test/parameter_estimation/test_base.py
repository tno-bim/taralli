"""
Tests related to the `taralli.parameter_estimation.base`.
"""

import numpy as np
import pytest
from scipy import optimize
from scipy.stats import norm

from taralli.parameter_estimation.base import (
    DynestyParameterEstimator,
    EmceeParameterEstimator,
    NestleParameterEstimator,
)
from taralli.parameter_estimation.utils import get_credible_region_2d
from taralli.parameter_estimation.weighted_sample_statistics import (
    weighted_sample_median_absolute_deviation,
)
from taralli.utilities.kde import kde_2d
from test.unit_test.parameter_estimation.utilities import (
    half_torus,
    mean_par_posterior_conjugate_normal_with_known_sd,
    torus_mass,
)

plot_fig = False


def test_sample_normal_distribution_emcee():
    """
    Sample a normal prior with emcee using a flat likelihood.
    """
    ndim = 1
    true_scale = 1
    true_loc = 5

    def log_prior(theta):
        return norm.logpdf(theta, loc=true_loc, scale=true_scale)

    def log_likelihood(theta):
        return np.ones(theta.shape, dtype=np.float64)

    nwalkers = 20
    emcee_model = EmceeParameterEstimator(
        nwalkers=nwalkers,
        ndim=ndim,
        log_likelihood=log_likelihood,
        log_prior=log_prior,
        sampling_initial_positions=45 + np.random.randn(nwalkers, ndim),
        nsteps=8_000,
        initial_nsteps=500,
    )
    emcee_model.estimate_parameters(vectorize=True)
    emcee_model.summary()
    median_estimate = emcee_model.summary_output["median"]
    posterior_sample = emcee_model.posterior_sample
    mad_estimate = weighted_sample_median_absolute_deviation(sample=posterior_sample)

    np.testing.assert_allclose(median_estimate, true_loc, atol=5e-2)
    np.testing.assert_allclose(mad_estimate, true_scale, atol=5e-2)


def test_sample_normal_distribution_nestle():
    """
    Sample a normal prior with nestle using a flat likelihood.
    """
    ndim = 1
    true_scale = 1
    true_loc = 5

    def prior_transform(theta):
        return norm.ppf(theta, loc=true_loc, scale=true_scale)

    def log_likelihood(theta):
        return np.ones(theta.shape, dtype=np.float64)

    nestle_model = NestleParameterEstimator(
        ndim=ndim, log_likelihood=log_likelihood, prior_transform=prior_transform
    )
    nestle_model.estimate_parameters(npoints=1000, dlogz=0.01)
    nestle_model.summary()
    median_estimate = nestle_model.summary_output["median"]
    posterior_sample = nestle_model.posterior_sample
    posterior_sample_weights = nestle_model.posterior_sample_weights
    mad_estimate = weighted_sample_median_absolute_deviation(
        sample=posterior_sample, weight_vec=posterior_sample_weights
    )
    log_evidence_estimate = nestle_model.posterior_sample_log_evidence

    np.testing.assert_allclose(median_estimate, true_loc, rtol=1e-1)
    np.testing.assert_allclose(mad_estimate, true_scale, rtol=1e-1)
    np.testing.assert_allclose(log_evidence_estimate, 1, rtol=1e-3)


def test_sample_normal_distribution_dynesty():
    """
    Sample a normal prior with nestle using a flat likelihood.
    """
    ndim = 1
    true_scale = 1
    true_loc = 5

    def prior_transform(theta):
        return norm.ppf(theta, loc=true_loc, scale=true_scale)

    def log_likelihood(theta):
        # we use this slightly sloped line because the recent `dynesty` does not
        # work with completely flat log-likelihoods
        return 1.0 - abs(float(theta)) * 100.0 * np.finfo(float).eps

    dynesty_model = DynestyParameterEstimator(
        ndim=ndim, log_likelihood=log_likelihood, prior_transform=prior_transform
    )
    dynesty_model.estimate_parameters(nlive=4000)
    dynesty_model.summary()
    median_estimate = dynesty_model.summary_output["median"]
    posterior_sample = dynesty_model.posterior_sample
    posterior_sample_weights = dynesty_model.posterior_sample_weights
    mad_estimate = weighted_sample_median_absolute_deviation(
        sample=posterior_sample, weight_vec=posterior_sample_weights
    )
    log_evidence_estimate = dynesty_model.posterior_sample_log_evidence

    np.testing.assert_allclose(median_estimate, true_loc, rtol=1e-1)
    np.testing.assert_allclose(mad_estimate, true_scale, rtol=1e-1)
    np.testing.assert_allclose(log_evidence_estimate[-1], 1, rtol=5e-3)


def test_pdf_torus_emcee():
    ndim = 2
    r_tube = 0.4
    r_major = 1 / ((np.pi * r_tube) ** 2)
    mass = 0.50

    x_vec = np.linspace(start=-5, stop=5, num=100)
    x_mx, y_mx = np.meshgrid(x_vec, x_vec)
    theta = np.hstack((x_mx.reshape(-1, 1), y_mx.reshape(-1, 1)))
    p_vec = half_torus(xy=theta, r_tube=r_tube, r_major=r_major)
    p_mx = p_vec.reshape(x_mx.shape)

    def fun(p):
        return torus_mass(p, r_tube) - mass

    p_star_expected = optimize.brentq(f=fun, a=0, b=np.max(p_mx))

    def log_likelihood(theta):
        return np.ones(theta.shape[0], dtype=np.float64)

    def log_prior(theta):
        theta = np.atleast_2d(theta)
        return np.log(half_torus(theta, r_tube=r_tube, r_major=r_major))

    nwalkers = 20
    model = EmceeParameterEstimator(
        nwalkers=nwalkers,
        ndim=ndim,
        log_likelihood=log_likelihood,
        log_prior=log_prior,
        sampling_initial_positions=[0, r_major] + np.random.randn(nwalkers, ndim) / 20,
        nsteps=15_000,
        initial_nsteps=1_000,
    )
    model.estimate_parameters(vectorize=True)
    model.summary()
    median_estimate = model.summary_output["median"]
    posterior_sample = model.posterior_sample
    bandwidth_posterior, density_mx_posterior, x_mx_posterior, y_mx_posterior = kde_2d(
        sample_mx=posterior_sample
    )

    xy_bds, p_star = get_credible_region_2d(
        x_mx=x_mx_posterior,
        y_mx=y_mx_posterior,
        density_mx=density_mx_posterior,
        prob_mass=mass,
    )

    if plot_fig:
        model.plot_posterior()

    np.testing.assert_allclose(median_estimate, np.array([0, 0]), rtol=5e-2, atol=5e-2)
    np.testing.assert_allclose(p_star, p_star_expected, rtol=5e-2, atol=5e-2)


# def test_pdf_torus_nestle():
#     ndim = 2
#     r_tube = 0.3
#     r_major = 1 / (np.pi * r_tube) ** 2
#     mass = 0.50
#
#     x_vec = np.linspace(start=-10, stop=10, num=200)
#     x_mx, y_mx = np.meshgrid(x_vec, x_vec)
#     theta = np.hstack((x_mx.reshape(-1, 1), y_mx.reshape(-1, 1)))
#     p_vec = half_torus(xy=theta, r_tube=r_tube, r_major=r_major)
#     p_mx = p_vec.reshape(x_mx.shape)
#
#     p_star_expected = optimize.brentq(
#         f=lambda p, r_tube: torus_mass(p, r_tube) - mass, a=0, b=np.max(p_mx),
#         args=(r_tube)
#     )
#
#     prior_tranform = lambda x: np.array([4. * x[0] - 2., 4. * x[1] - 2.])
#
#     def log_likelihood(theta):
#         theta = np.atleast_2d(theta)
#         return np.log(half_torus(theta, r_tube=r_tube, r_major=r_major))
#
#
#     model = NestleParameterEstimator(
#         ndim=ndim,
#         log_likelihood=log_likelihood,
#         prior_transform=prior_tranform
#     )
#     model.estimate_parameters(npoints=1000, method='multi', dlogz=0.05)
#     model.summary()
#
#     model.plot_posterior()
#     median_actual = model.summary_output["median"]
#     posterior_sample = model.posterior_sample
#     (
#     bandwidth_posterior, density_mx_posterior, x_mx_posterior, y_mx_posterior
#     ) = kde_2d(
#         sample_mx=posterior_sample
#     )
#
#     xy_bds, p_star = get_credible_region_2d(x_mx=x_mx_posterior, y_mx=y_mx_posterior,
#       density_mx=density_mx_posterior, prob_mass=mass)
#
#     if plot_fig:
#         plt.subplots()
#         plt.contourf(x_mx_posterior, y_mx_posterior, density_mx_posterior)
#         plt.axis("equal")
#         plt.show()
#
#     np.testing.assert_allclose(median_actual, np.array([0, 0]), atol=5e-2)
#     np.testing.assert_allclose(p_star, p_star_expected, atol=5e-2)


def run_conjugate_prior(Estimator):
    x_measured = np.array([30.1, 28.7, 21.2, 28.6, 25.8, 25.1, 24.2, 27.3, 20.0, 34.8])
    std = 5
    mu_0 = 40
    std_0 = 100

    (
        mean_par_posterior_mean_expected,
        mean_par_posterior_std_expected,
    ) = mean_par_posterior_conjugate_normal_with_known_sd(
        mean_par_prior_mean=mu_0,
        mean_par_prior_std=std_0,
        sample=x_measured,
        known_std=std,
    )

    def log_likelihood(theta):
        theta = np.array(theta).reshape((-1, 1))
        return np.sum(norm.logpdf(x=x_measured, loc=theta, scale=std), axis=1)[
            :, np.newaxis
        ]

    prior = norm(loc=mu_0, scale=std_0)

    if Estimator.__name__.startswith("Emcee"):

        def log_prior(theta):
            theta = theta.reshape((-1, 1))
            return prior.logpdf(theta)

        nwalkers = 20
        model = Estimator(
            nwalkers=nwalkers,
            ndim=1,
            log_likelihood=log_likelihood,
            log_prior=log_prior,
            sampling_initial_positions=np.mean(x_measured)
            + np.random.randn(nwalkers, 1),
            nsteps=5_000,
            initial_nsteps=500,
            seed=6174,
        )
        model.estimate_parameters(vectorize=True)
    else:

        def prior_transform(x):
            return prior.ppf(x)

        model = Estimator(
            ndim=1,
            # these samplers are not vectorized so we can do this
            log_likelihood=lambda theta: log_likelihood(theta).ravel()[0],
            prior_transform=prior_transform,
            seed=6174,
        )
        model.estimate_parameters(npoints=500, dlogz=0.01)

    model.summary()

    mean_par_posterior_mean_estimate = model.summary_output["mean"]
    mean_par_posterior_std_estimate = np.sqrt(model.summary_output["covariance"])

    if plot_fig:
        model.plot_posterior()
        model.plot_prior()

    np.testing.assert_allclose(
        mean_par_posterior_mean_estimate, mean_par_posterior_mean_expected, rtol=5e-2
    )
    np.testing.assert_allclose(
        mean_par_posterior_std_estimate, mean_par_posterior_std_expected, rtol=5e-2
    )


def test_conjugate_prior_emcee():
    run_conjugate_prior(EmceeParameterEstimator)


def test_conjugate_prior_nestle():
    run_conjugate_prior(NestleParameterEstimator)


def test_conjugate_prior_dynesty():
    run_conjugate_prior(DynestyParameterEstimator)


def test_incorrect_initial_position_dim():
    ndim = 1
    sip2 = np.array([2, 4])

    def log_prior(theta):
        return norm.logpdf(theta)

    def log_likelihood(theta):
        return np.ones(theta.shape, dtype=np.float64)

    nwalkers = 5
    stat_model = EmceeParameterEstimator(
        nwalkers=nwalkers,
        ndim=ndim,
        log_likelihood=log_likelihood,
        log_prior=log_prior,
        sampling_initial_positions=sip2 + np.random.randn(nwalkers, 1) / 10,
        nsteps=1_000,
        initial_nsteps=100,
    )

    with pytest.raises(Exception):
        assert stat_model.estimate_parameters()


def test_kl_divergence():
    ndim = 1
    true_scale = 1
    true_loc = 5

    def prior_transform(theta):
        return norm.ppf(theta, loc=true_loc, scale=true_scale)

    def log_likelihood(theta):
        # we use this slightly sloped line because the recent `dynesty` does not
        # work with completely flat log-likelihoods
        return 1.0 - abs(float(theta)) * 100.0 * np.finfo(float).eps

    dynesty_model = DynestyParameterEstimator(
        ndim=ndim, log_likelihood=log_likelihood, prior_transform=prior_transform
    )
    dynesty_model.estimate_parameters(nlive=3000)

    assert dynesty_model.kl_divergence[-1] == pytest.approx(0, abs=0.005)
