import matplotlib.pyplot as plt
import numpy as np

plot_fig = False

#
# def __triangle_fun(x, a, b):
#     idx_left_right = np.logical_or(x < a, x > b)
#     idx_mid_left =
#     idx_mid = ~np.logical_or(idx_left, idx_right)
#     y = np.empty(x.shape)
#     y[idx_left_right] = 0
#
#     return y


def triangle_grid(a, b, num: int = 100, left_trunc=None, x_lb=None, x_ub=None, x=None):
    """Creates an isosceles triangular probability distribution.

    un-truncated:
              /\
    _ _ _ _ _/  \\ _ _ _ _ _
            a    b

    left truncated:
              /\
    _ _ _ _ _|  \\ _ _ _ _ _
    left_trunc  b

    Args:
        a: first coordinate of the triangle`s base.
        b: second coordinate coordinate of the triangle`s base.
        num: number of elements in the discretized probability distribution.
        left_trunc: position of left truncation point.
        x_lb: lower endpoint of `x`. If not specified it is half of the base of
            triangle to left of the triangle.
        x_ub: upper endpoint of `x`. If not specified it is half of the base of
            triangle to right of the triangle.
        x: discretized support of the distribution. If this is used then `num`,
            `x_lb`, and `x_ub` are ignored and `p` is evaluated over `x`.

    Returns:
        x: discretized support of the distribution.
        p: density values corresponding to `x`.
    """
    base = b - a
    height = (b - a) / 2

    if x_lb is None:
        x_lb = a - base / 2
    if x_lb > a:
        raise ValueError

    if x_ub is None:
        x_ub = b + base / 2
    if x_ub < b:
        raise ValueError

    if x is None:
        x = np.linspace(start=x_lb, stop=x_ub, num=num)
        x = np.sort(np.hstack((x, (a + b) / 2)))
        if left_trunc is not None:
            eps = np.finfo(float).eps * left_trunc * 1.1
            if left_trunc >= b:
                raise ValueError("`left_trunc` must be smaller than `b`.")
            x = np.sort(np.hstack((x, left_trunc - eps, left_trunc + eps)))

    p = np.interp(
        x, xp=[a, a + base / 2, b], fp=[0.0, height, 0.0], left=0.0, right=0.0
    )
    if left_trunc is not None:
        idx = x < left_trunc
        p[idx] = 0.0
        area = np.trapz(p, x)
        p = 1 / area * p

    return x, p


def multiplication_factor_area_triangle(a):
    """Computes multiplication factor for cut triangles in order to have the area = 1.

    Args:
        a: first coordinate of the triangle`s base.

    Returns:
        multipl_factor: multiplication factor
    """
    area_loss = a ** 2 / 2
    multipl_factor = 1 / (1 - area_loss)
    return multipl_factor


def wedge_grid(a, b, s=0.1):
    """Creates a 2D probability distribution shaped as a wedge.

                / |
               /| |
    _ _ _ _ _ / | |_ _ _ _ _
    _ _ _ _ _/  |/_ _ _ _ _
             a  b

    Args:
        a: first coordinate of the triangle`s base.
        b: second coordinate coordinate of the triangle`s base.
        s: discretization.

    Returns:
        x_mx, y_mx, z_mx: grid-coordinates of the probability distribution.
    """

    x_vec = np.arange(start=a, stop=b + s, step=s)
    z_max = 2 / (b - a) ** 2
    z_vec = np.linspace(start=0, stop=z_max, num=len(x_vec))
    x_mx, y_mx = np.meshgrid(x_vec, x_vec)
    z_mx = np.tile(z_vec, (len(x_vec), 1))
    return x_mx, y_mx, z_mx


def find_corner(x_pos, y_pos, x, y):
    atol = 1e-8
    idx = np.argwhere(
        np.logical_and(np.isclose(x, x_pos, atol=atol), np.isclose(y, y_pos, atol=atol))
    )
    corner = np.hstack((x[idx], y[idx]))
    if corner.size != 0:
        corner = np.unique(corner.round(decimals=6), axis=0)
    return corner


def bivariate_gaussian_mass(r):
    """
    Calculate the probability mass of an independent standard normal bivariate
    probability distribution (pdf) falling inside a circle (boundary circle) centered
    in the origin with radius `r`.

    Args:
        r: radius of the boundary circle.

    Returns:
        prob_mass: probability mass encompassed by the boundary circle
    """
    p_r = 1 / (2 * np.pi) * np.exp(-1 / 2 * r ** 2)
    p_max = 1 / (2 * np.pi)
    # prob_mass_1 = np.pi * integrate.quad_vec(
    #   lambda p: -2 * np.log(2 * np.pi * p), p_r, p_max
    # )[0]
    prob_mass_1 = (
        -2
        * np.pi
        * (
            p_max * (np.log(2 * np.pi * p_max) - 1)
            - p_r * (np.log(2 * np.pi * p_r) - 1)
        )
    )
    prob_mass_2 = p_r * r ** 2 * np.pi
    prob_mass = prob_mass_1 + prob_mass_2
    return prob_mass


def half_circle(x, r, c):
    """
    ^
    |        _ _
    |      /     \
    |_ _ _|       | _ _ _
     <---c--->
          <--2r-->
    Args:
        x: point or array where the function is estimated.
        r: radius of the circle.
        c: coordinate of the center of the circle.

    Returns:
        y: y-coordinate of the half circle centered in `c` and with radius`r_tube`.

    """
    idx_mid = np.argwhere(np.logical_and(x >= c - r, x <= c + r))
    y = np.zeros(x.shape)
    y[idx_mid] = np.sqrt(r ** 2 - (x[idx_mid] - c) ** 2)

    if plot_fig:
        plt.subplots()
        plt.plot(x, y, "bo")
        plt.axis("equal")
        plt.show()

    return y


def torus_mass(p, r_tube):
    """
    Calculate the probability mass of a half torus probability distribution (pdf)
    falling between two concentric circles (`r_inner` and `r_outer` centered in the
    origin of the torus and determined by cutting the torus with a horizontal plane
    at the height of `p`. The major radius of the torus is needed as that is uniquely
    determined by `r_tube` and that the volume should be 1 (proper distribution).

    Args:
        p: height of the horizontal cutting plane that defines `r_inner` and `r_outer`.
        r_tube: tube radius of the torus.

    Returns:
        prob_mass: probability mass encompassed by the two concentric circles.
    """
    if p <= 0:
        prob_mass = 1.0
    elif p >= r_tube:
        prob_mass = 0.0
    else:
        r_major = 1 / ((np.pi * r_tube) ** 2)

        r_prime = np.sqrt(r_tube ** 2 - p ** 2)
        area_circular_segment = r_tube * (
            r_tube * np.arccos(p / r_tube) - p * np.sqrt(1 - p ** 2 / r_tube ** 2)
        )
        area_rectangle = 2 * p * r_prime
        prob_mass = 2 * np.pi * r_major * (area_circular_segment + area_rectangle)

    return prob_mass


def half_torus(xy, r_tube, r_major):
    """

    Args:
        xy:
        r_tube: tube radius of the torus (minor radius).
        r_major: major radius of the torus.

    Returns:
        z: z-coordinate of the torus.

    """
    radius_xy = np.sqrt(xy[:, 0] ** 2 + xy[:, 1] ** 2)
    z = half_circle(radius_xy, r_tube, r_major)
    return z


def mean_par_posterior_conjugate_normal_with_known_sd(
    mean_par_prior_mean, mean_par_prior_std, sample, known_std
):
    sample = sample.ravel()
    n_sample = len(sample)
    mean_par_posterior_std = np.sqrt(
        1 / (n_sample / known_std ** 2 + 1 / mean_par_prior_std ** 2)
    )
    mean_par_posterior_mean = mean_par_posterior_std ** 2 * (
        mean_par_prior_mean / mean_par_prior_std ** 2 + sample.sum() / known_std ** 2
    )
    return mean_par_posterior_mean, mean_par_posterior_std
