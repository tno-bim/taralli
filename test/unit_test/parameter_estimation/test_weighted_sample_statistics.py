import numpy as np
import pytest
from scipy.stats import median_abs_deviation

from taralli.parameter_estimation.weighted_sample_statistics import (
    weighted_sample_covariance,
    weighted_sample_median_absolute_deviation,
    weighted_sample_quantile,
)


def test_weighted_sample_covariance():
    """
    Example from r_major cov.wt:
    https://stat.ethz.ch/R-manual/R-patched/library/stats/html/cov.wt.html
    """
    sample = np.array(
        [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10], [1, 2, 3, 8, 7, 6, 5, 8, 9, 10]]
    ).T
    weights = np.array([0, 0, 0, 1, 1, 1, 1, 1, 0, 0])
    cov_expected = np.array([[2.5, -0.5], [-0.5, 1.7]])

    cov = weighted_sample_covariance(sample=sample, weight_vec=weights)
    np.testing.assert_allclose(cov, cov_expected)


def test_weighted_1d_sample_quantile_no_zero_weight():
    """
    Tested against the r_major package Hmisc, for the weighted variant.
    (wtd.quantile(type='(i-1)/(n-1)'),
    https://cran.r-project.org/web/packages/Hmisc/index.html.
    """
    sample = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    weights = np.array([1, 1, 1, 2, 2, 2, 2, 2, 1, 1])
    weights_ones = np.ones(sample.shape[0])
    quantile_prob = [0, 0.01, 0.5, 0.8, 0.9, 0.99, 1]

    quantile_val_expected_w = np.array([1.00, 1.14, 5.50, 7.60, 8.60, 9.86, 10.00])

    # no weight_vec
    quantile_val_nw = weighted_sample_quantile(
        sample=sample, quantile_prob=quantile_prob, weight_vec=None
    )
    quantile_val_expected_nw = np.quantile(sample, q=quantile_prob)
    np.testing.assert_allclose(quantile_val_nw.squeeze(), quantile_val_expected_nw)

    # equal weight_vec
    quantile_val_ew = weighted_sample_quantile(
        sample=sample, quantile_prob=quantile_prob, weight_vec=weights_ones
    )
    np.testing.assert_allclose(quantile_val_ew.squeeze(), quantile_val_expected_nw)

    # with weight_vec
    quantile_w = weighted_sample_quantile(
        sample=sample, quantile_prob=quantile_prob, weight_vec=weights
    )
    np.testing.assert_allclose(quantile_w.squeeze(), quantile_val_expected_w)


def test_weighted_2d_sample_quantile():
    """
    Tested against the r_major package Hmisc, for the weighted variant.
    (wtd.quantile(type='(i-1)/(n-1)'),
    https://cran.r-project.org/web/packages/Hmisc/index.html.
    """
    sample = np.tile(np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]).reshape((-1, 1)), (1, 2))
    weights = np.array([1, 1, 1, 2, 2, 2, 2, 2, 1, 1])
    quantile_prob = [0, 0.01, 0.5, 0.8, 0.9, 0.99, 1]

    quantile_val_expected_w = np.tile(
        np.array([1.00, 1.14, 5.50, 7.60, 8.60, 9.86, 10.00]).reshape((-1, 1)), (1, 2)
    )

    # with weight_vec
    quantile_w = weighted_sample_quantile(
        sample=sample, quantile_prob=quantile_prob, weight_vec=weights
    )
    np.testing.assert_allclose(quantile_w.squeeze(), quantile_val_expected_w)


def test_weighted_1d_sample_quantile_with_zero_weight():
    """
    Tested against the r_major package Hmisc, for the weighted variant.
    (wtd.quantile(type='(i-1)/(n-1)'),
    https://cran.r-project.org/web/packages/Hmisc/index.html.
    """
    sample = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    weights = np.array([0, 0, 0, 1, 1, 1, 1, 1, 0, 0])
    quantile_prob = [0, 0.01, 0.5, 0.8, 0.9, 0.99, 1]

    quantile_val_expected_w = np.array([4.00, 4.04, 6.00, 7.20, 7.60, 7.96, 8.00])

    # with weight_vec
    quantile_w = weighted_sample_quantile(
        sample=sample, quantile_prob=quantile_prob, weight_vec=weights
    )
    np.testing.assert_allclose(quantile_w.squeeze(), quantile_val_expected_w)


def test_weighted_sample_quantile_wrong_quantile_prob_input():
    with pytest.raises(ValueError):
        weighted_sample_quantile(sample=[1, 2], quantile_prob=1.1)


def test_weighted_sample_quantile_wrong_weights_input():
    with pytest.raises(ValueError):
        weighted_sample_quantile(sample=[1, 2], quantile_prob=0.5, weight_vec=[1])


def test_weighted_sample_mad_no_weights_1d():
    sample = np.random.normal(size=1_000)
    mad_np = median_abs_deviation(x=sample, scale="normal")
    mad_test = weighted_sample_median_absolute_deviation(sample=sample)

    np.testing.assert_allclose(mad_test, mad_np)


def test_weighted_sample_mad_no_weights_2d():
    sample = np.hstack(
        (
            np.random.normal(size=1_000, loc=5, scale=3).reshape(-1, 1),
            np.random.normal(size=1_000).reshape(-1, 1),
        )
    )
    mad_np = median_abs_deviation(x=sample, scale="normal")
    mad_test = weighted_sample_median_absolute_deviation(sample=sample)

    np.testing.assert_allclose(mad_test, np.atleast_2d(mad_np))
