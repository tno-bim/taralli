"""
Tests related to the functions in parameter_estimation.utils except the ones related
to credible regions.

TODO: - pytest fixed sample_vec decorator
      - rewrite triangle_fun
"""

import numpy as np
from scipy import integrate

from taralli.parameter_estimation.utils import trapz2


# ======================================================================================
# DOUBLE INTEGRATION FROM SAMPLE
# ======================================================================================
def test_trapz2():
    n_x = 100
    n_y = 50
    x_vec = np.linspace(-2, 2, num=n_x)
    y_vec = np.linspace(-1, 1, num=n_y)

    def fun(x, y):
        return x ** 2 + y ** 2

    x_mx, y_mx = np.meshgrid(x_vec, y_vec)
    z_mx = fun(x_mx, y_mx)
    q = trapz2(x_vec, y_vec, z_mx)

    q_expected = integrate.dblquad(fun, -2, 2, lambda x: -1, lambda x: 1)[0]
    q_matlab = 13.337731278656566
    np.testing.assert_allclose(q, q_expected, atol=1e-2)
    np.testing.assert_allclose(q, q_matlab)
