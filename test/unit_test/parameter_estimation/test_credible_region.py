"""
Tests the functions computing credible regions.
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy import optimize, stats

from taralli.parameter_estimation.utils import (
    get_credible_region_1d,
    get_credible_region_2d,
)
from test.unit_test.parameter_estimation.utilities import (
    bivariate_gaussian_mass,
    find_corner,
    half_torus,
    multiplication_factor_area_triangle,
    torus_mass,
    triangle_grid,
    wedge_grid,
)

plot_fig = False


# ======================================================================================
# 1D  CREDIBLE REGION
# ======================================================================================
def test_credible_region_1d_triangle():
    xx, zz = triangle_grid(1, 3, num=100_000)
    mass = 0.90
    p_star_analytic = np.sqrt(1 - mass)
    x_boundaries_analytic = np.array([[1 + p_star_analytic, 3 - p_star_analytic]])
    x_boundaries, p_star = get_credible_region_1d(xx, zz, mass)
    np.testing.assert_allclose(p_star[0], p_star_analytic, atol=1e-4)
    np.testing.assert_allclose(x_boundaries[mass], x_boundaries_analytic, atol=1e-4)


def test_credible_region_1d_triangle_wall():
    a, b = -0.5, 1.5
    xx, zz = triangle_grid(a, b, num=100_000, left_trunc=0.0)
    mf = multiplication_factor_area_triangle(a)
    mass = 0.95
    p_star_analytic = np.sqrt(2) * np.sqrt(mf * (1 - mass))
    x_boundaries_analytic = np.array([[0, 0.5 + (mf - p_star_analytic) / mf]])
    x_boundaries, p_star = get_credible_region_1d(xx, zz, mass)
    np.testing.assert_allclose(p_star[0], p_star_analytic, atol=1e-4)
    np.testing.assert_allclose(x_boundaries[mass], x_boundaries_analytic, atol=1e-4)


def test_credible_region_1d_double_triangle():
    a1, b1 = 1, 3
    a2, b2 = 4, 6
    xx1, zz1 = triangle_grid(a1, b1, num=100_000, x_lb=a1 - 1.0, x_ub=b2 + 1.0)
    _, zz2 = triangle_grid(a2, b2, x=xx1)
    zz = (zz1 + zz2) / 2
    mass = 0.90
    p_star_analytic = np.sqrt(1 - mass) / 2
    x_boundaries_analytic = np.array(
        [
            [a1 + 2 * p_star_analytic, b1 - 2 * p_star_analytic],
            [a2 + 2 * p_star_analytic, b2 - 2 * p_star_analytic],
        ]
    )
    x_boundaries, p_star = get_credible_region_1d(xx1, zz, mass)
    np.testing.assert_allclose(p_star[0], p_star_analytic, atol=1e-4)
    np.testing.assert_allclose(x_boundaries[mass], x_boundaries_analytic, atol=1e-4)


def test_get_credible_region_1d_for_normal_distribution():
    n_sim = int(1e4)
    mass = 0.9
    xx = np.random.sample(n_sim) * (+4 - (-4)) - 4
    xx = np.sort(xx)
    zz = stats.norm.pdf(xx)

    k = sp.stats.norm.ppf(q=(1 + mass) / 2, loc=0, scale=1)
    expected_x_boundaries = np.array([-k, k])

    x_boundaries, p_star = get_credible_region_1d(
        x_vec=xx, density_vec=zz, prob_mass=mass
    )

    np.testing.assert_allclose(
        x_boundaries[mass].ravel(), expected_x_boundaries, rtol=1e-01
    )


def test_credible_region_1d_for_normal_bimodal_distribution():
    n_sim = int(1e4)
    mass = 0.9
    xx1 = np.random.sample(n_sim) * (+4 - (-4)) - 4
    xx1 = np.sort(xx1)
    zz1 = stats.norm.pdf(xx1)

    xx2 = np.random.sample(n_sim) * (13 - 5) + 5
    xx2 = np.sort(xx2)
    zz2 = stats.norm.pdf(xx2, loc=9)
    xx = np.hstack((xx1, xx2))
    zz = np.hstack((zz1, zz2)) / 2

    k = stats.norm.ppf(q=(1 + mass) / 2, loc=0, scale=1)
    expected_x_boundaries = np.array([[-k, k], [-k + 9, k + 9]])

    x_boundaries, p_star = get_credible_region_1d(
        x_vec=xx, density_vec=zz, prob_mass=mass
    )

    np.testing.assert_allclose(expected_x_boundaries, x_boundaries[mass], rtol=5e-01)


# ======================================================================================
# 2D  CREDIBLE REGION
# ======================================================================================
def test_credible_region_2d_wedge():
    cbrt2 = np.cbrt(2)
    a, b = 1, 1 + cbrt2
    s = 0.01

    mass = 0.90
    p_star_analytical = np.sqrt((1 - mass) * 2 / cbrt2)

    x_mx, y_mx, z_mx = wedge_grid(a, b, s)

    if plot_fig:

        plt.subplots()
        plt.contour(x_mx, y_mx, z_mx)
        plt.plot(x_mx, y_mx, "bo", markersize=1)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        ax.scatter(x_mx, y_mx, z_mx)

    corners_analytical = np.array(
        [[a + p_star_analytical, a], [b, a], [b, b], [a + p_star_analytical, b]]
    )

    xy_boundaries, p_star = get_credible_region_2d(x_mx, y_mx, z_mx, mass)

    xy = xy_boundaries[mass][0]
    x = xy[:, 0]
    y = xy[:, 1]
    x_min, x_max = np.min(x), np.max(x)
    y_min, y_max = np.min(y), np.max(y)

    corner_top_left = find_corner(x_min, y_max, x, y)
    corner_bottom_left = find_corner(x_min, y_min, x, y)
    corner_top_right = find_corner(x_max, y_max, x, y)
    corner_bottom_right = find_corner(x_max, y_min, x, y)

    corners = np.vstack(
        (corner_bottom_left, corner_bottom_right, corner_top_right, corner_top_left)
    )

    np.testing.assert_allclose(p_star, p_star_analytical, atol=5e-3)
    np.testing.assert_allclose(corners, corners_analytical, atol=5e-2)

    if plot_fig:

        plt.subplots()
        plt.plot(x, y, "-o")
        plt.show()
        plt.axis("equal")
        plt.show()


def test_credible_region_2d_std_normal():
    n_step = 100
    x = np.linspace(-5, 5, n_step)
    y = np.linspace(-5, 5, n_step)
    x_mx, y_mx = np.meshgrid(x, y)
    xy_mx = np.hstack((x_mx.reshape(-1, 1), y_mx.reshape(-1, 1)))
    p = stats.multivariate_normal.pdf(xy_mx, mean=[0, 0])
    p_prime = p.reshape(x_mx.shape)

    mass = 0.9

    r = optimize.brentq(
        f=lambda r: bivariate_gaussian_mass(r) - mass,
        a=0,
        b=0.5 + 2 * stats.norm.ppf((1 + mass) / 2),
    )

    xy_bds, p_star = get_credible_region_2d(x_mx, y_mx, p_prime, prob_mass=0.90)
    bds = xy_bds[mass][0]
    r_estimate = np.sqrt(bds[:, 0] ** 2 + bds[:, 1] ** 2)
    r_estimate_median = np.median(r_estimate)

    np.testing.assert_allclose(r_estimate_median, r, atol=5e-3)


def test_credible_region_2d_multimodal():
    n_step = 100
    x = np.linspace(-5, 15, n_step)
    y = np.linspace(-5, 15, n_step)
    x_mx, y_mx = np.meshgrid(x, y)
    xy_mx = np.hstack((x_mx.reshape(-1, 1), y_mx.reshape(-1, 1)))
    p = (
        stats.multivariate_normal.pdf(xy_mx, mean=[0, 0]) / 2
        + stats.multivariate_normal.pdf(xy_mx, mean=[5, 10]) / 2
    )
    p_prime = p.reshape(x_mx.shape)

    mass = 0.9

    r1 = optimize.brentq(
        f=lambda r: bivariate_gaussian_mass(r) - mass,
        a=0,
        b=0.5 + 2 * stats.norm.ppf((1 + mass) / 2),
    )

    xy_bds, p_star = get_credible_region_2d(x_mx, y_mx, p_prime, prob_mass=mass)

    xy_bd1 = xy_bds[mass][0]
    r1_estim = np.sqrt(xy_bd1[:, 0] ** 2 + xy_bd1[:, 1] ** 2)
    assert np.max(np.abs(np.diff(r1_estim))) < 1e-2
    r1_estimate_median = np.mean(r1_estim)

    c2 = np.array([5, 10])
    xy_bd2 = xy_bds[mass][1]
    r2_estim = np.sqrt((xy_bd2[:, 0] - c2[0]) ** 2 + (xy_bd2[:, 1] - c2[1]) ** 2)
    assert np.max(np.abs(np.diff(r2_estim))) < 1e-2
    r2_estimate_median = np.median(r2_estim)
    r_estimate_median = [r1_estimate_median, r2_estimate_median]

    r = [r1, r1]

    np.testing.assert_allclose(r_estimate_median, r, atol=5e-2)

    if plot_fig:
        plt.subplots()
        plt.contourf(x_mx, y_mx, p_prime)
        plt.plot(xy_bd1[:, 0], xy_bd1[:, 1])
        plt.plot(xy_bd2[:, 0], xy_bd2[:, 1])
        plt.show()


def test_credible_region_2d_half_torus():
    mass = 0.40
    r_tube = 0.2
    r_major = 1 / ((np.pi * r_tube) ** 2)
    n_discr = 100

    x = np.linspace(-5, 5, num=n_discr)
    x_mx, y_mx = np.meshgrid(x, x)
    xy = np.hstack((x_mx.reshape(-1, 1), y_mx.reshape(-1, 1)))
    p = half_torus(xy, r_tube, r_major)
    p_prime = p.reshape(x_mx.shape)

    if plot_fig:
        plt.subplots()
        plt.contourf(x_mx, y_mx, p_prime)
        plt.axis("equal")
        # plt.plot(X, Y, 'go')
        plt.show()

    def fun(p):
        return torus_mass(p, r_tube) - mass

    p_star_expected = optimize.brentq(f=fun, a=0, b=np.max(p_prime))

    idx, idy = np.nonzero(p_prime > p_star_expected)
    x_cr_mx, y_cr_mx = x_mx[idx, idy], y_mx[idx, idy]
    xy_bds, p_star = get_credible_region_2d(x_mx, y_mx, p_prime, prob_mass=mass)
    # idx_check, idy_check = np.nonzero(p_prime > p_star)
    # X_cr_ch, Y_cr_ch = X[idx_check, idy_check], Y[idx_check, idy_check]

    if plot_fig:
        plt.subplots()
        plt.contourf(x_mx, y_mx, p_prime)
        plt.plot(x_cr_mx, y_cr_mx, "ro")
        for island in xy_bds[mass]:
            plt.plot(island[:, 0], island[:, 1])
        # plt.plot(X_cr_ch, Y_cr_ch, "bo", markersize=0.8)
        plt.show()

    np.testing.assert_allclose(p_star, p_star_expected, atol=5e-2)
