""""
np.testing.assert_allclose
atol + rtol * abs(desired)

TODO:
    * changing the mesh generation would break this code -> interpolation would
        solve the potential problem of the mismatch of meshed, to be added later.
    * extend tests to cover branches.
"""
import os

import matplotlib.pyplot as plt
import numpy as np
import pytest
from scipy.interpolate import interp1d
from scipy.stats import gaussian_kde, norm

from taralli.utilities.kde import dct2d, hist_2d, idct2d, kde_1d, kde_2d

TEST_DATA_BASE_URL = "https://prob-taralli-data.s3.eu-central-1.amazonaws.com/kde/"
plot_fig = False


# ======================================================================================
# 1D
# ======================================================================================
def test_kde_1d_gaussian_mixture_against_matlab():
    sample = np.loadtxt(
        fname=os.path.join(
            TEST_DATA_BASE_URL, "1d_sample%23gaussian_mixture%23matlab.txt"
        )
    )
    expected_density_vec = np.loadtxt(
        fname=os.path.join(
            TEST_DATA_BASE_URL, "1d_density%23gaussian_mixture%23matlab.txt"
        )
    )
    expected_x_vec = np.loadtxt(
        fname=os.path.join(
            TEST_DATA_BASE_URL, "1d_x_mesh%23gaussian_mixture%23matlab.txt"
        )
    )

    _, density_vec, x_vec = kde_1d(
        sample_vec=sample, n_x_vec=2 ** 14, x_min=-15, x_max=15
    )

    np.testing.assert_allclose(x_vec, expected_x_vec, atol=1e-3)
    np.testing.assert_allclose(density_vec, expected_density_vec, atol=5e-3)

    if plot_fig is True:
        plt.subplots()
        plt.plot(expected_x_vec, expected_density_vec, label="expected (Matlab)")
        plt.plot(x_vec, density_vec, "--r", label="kde.py (taralli)")
        plt.legend()
        plt.show()


def test_kde_1d_gaussian_against_scipy():
    """
    We compare only against a unimodal distribution as "bimodal or multi-modal
    distributions tend to be oversmoothed" by `scipy.stats.gaussian_kde`, i.e. it is
    not a reliable reference solution.

    """
    np.random.seed(42)
    sample = norm.rvs(size=1000)

    _, density_vec, x_vec = kde_1d(sample_vec=sample)

    # scipy
    gkde = gaussian_kde(dataset=sample)
    expected_density_vec = gkde.evaluate(x_vec)

    np.testing.assert_allclose(density_vec, expected_density_vec, atol=1e-2)

    if plot_fig is True:
        plt.subplots()
        plt.plot(x_vec, expected_density_vec, label="scipy.stats.gaussian_kde")
        plt.plot(x_vec, density_vec, "--r_tube", label="kde.py (taralli)")
        plt.legend()
        plt.show()


def test_kde_1d_gaussian_with_weights_against_scipy():
    """
    We compare only against a unimodal distribution as "bimodal or multi-modal
    distributions tend to be oversmoothed" by `scipy.stats.gaussian_kde`, i.e. it is
    not a reliable reference solution.
    """
    np.random.seed(42)
    n_sample = 2000
    sample = norm.rvs(size=n_sample)
    weights = np.ones(n_sample)
    weights[0 : int(n_sample / 3)] = 0.5
    sample = np.sort(sample)

    _, density_vec, x_vec = kde_1d(
        sample_vec=sample, n_x_vec=2 ** 14, x_min=-5, x_max=5, weight_vec=weights
    )
    _, density_vec_nw, x_vec_nw = kde_1d(
        sample_vec=sample, n_x_vec=2 ** 14, x_min=-5, x_max=5
    )
    # scipy
    gkde = gaussian_kde(dataset=sample, weights=weights)
    expected_density_vec = gkde.evaluate(x_vec)

    np.testing.assert_allclose(density_vec, expected_density_vec, atol=1e-2)

    if plot_fig is True:
        plt.subplots()
        plt.plot(x_vec, expected_density_vec, label="scipy.stats.gaussian_kde")
        plt.plot(x_vec, density_vec, "--r", label="kde.py (taralli)")
        plt.plot(x_vec_nw, density_vec_nw, "--g", label="kde.py (taralli); no weight")
        plt.legend()
        plt.show()


def test_kde_1d_no_boundaries():
    np.random.seed(42)
    sample = np.random.randn(1000)

    _, density_vec_b, x_vec_b = kde_1d(sample_vec=sample, x_min=-5, x_max=5)
    _, density_vec_nb, x_vec_nb = kde_1d(sample_vec=sample)

    interp_fun = interp1d(x_vec_b, density_vec_b)
    density_vec_nb_interp = interp_fun(x_vec_nb)

    np.testing.assert_allclose(density_vec_nb_interp, density_vec_nb, atol=1e-4)


def test_kde_1d_wrong_weights_input():
    with pytest.raises(ValueError):
        sample = np.random.random(5)
        kde_1d(sample_vec=sample, weight_vec=[1, 2])
    with pytest.raises(ValueError):
        sample = np.random.random(3)
        kde_1d(sample_vec=sample, weight_vec=[-1, 2, 3])


# ======================================================================================
# 2D
# ======================================================================================
def test_kde_2d_simple_against_matlab():
    x = [0.11, 0.21, 0.31, 0.31, 0.21]
    y = [0.61, 0.31, 0.91, 0.91, 0.31]
    sample = np.vstack((x, y)).T
    _, density_mx, x_mx, y_mx = kde_2d(
        sample_mx=sample, n_row_mx=4, xy_min=[0, 0], xy_max=[1, 1]
    )

    expected_x_mx = np.array(
        [
            [0, 3.333333333333333e-01, 6.666666666666667e-01, 1.000000000000000e00],
            [0, 3.333333333333333e-01, 6.666666666666667e-01, 1.000000000000000e00],
            [0, 3.333333333333333e-01, 6.666666666666667e-01, 1.000000000000000e00],
            [0, 3.333333333333333e-01, 6.666666666666667e-01, 1.000000000000000e00],
        ]
    )
    expected_y_mx = np.array(
        [
            [0, 0, 0, 0],
            [
                3.333333333333333e-01,
                3.333333333333333e-01,
                3.333333333333333e-01,
                3.333333333333333e-01,
            ],
            [
                6.666666666666667e-01,
                6.666666666666667e-01,
                6.666666666666667e-01,
                6.666666666666667e-01,
            ],
            [
                1.000000000000000e00,
                1.000000000000000e00,
                1.000000000000000e00,
                1.000000000000000e00,
            ],
        ]
    )
    expected_density_mx = np.array(
        [
            [
                1.656375795026443e00,
                6.405166075308758e-01,
                8.212670025790514e-02,
                4.601619765514396e-03,
            ],
            [
                2.319920561403144e00,
                1.067794861703838e00,
                2.156967165887120e-01,
                1.831421342813525e-02,
            ],
            [
                2.427512383180407e00,
                1.658453039663006e00,
                5.448587632946583e-01,
                5.679296900586699e-02,
            ],
            [
                2.093705214644155e00,
                2.192159811306726e00,
                9.192163046403170e-01,
                1.019544385602958e-01,
            ],
        ]
    )

    np.testing.assert_allclose(x_mx, expected_x_mx, atol=1e-3)
    np.testing.assert_allclose(y_mx, expected_y_mx, atol=1e-3)
    np.testing.assert_allclose(density_mx, expected_density_mx, atol=5e-3)

    if plot_fig is True:
        plt.subplots()
        plt.contour(expected_x_mx, expected_y_mx, expected_density_mx, 5)
        plt.contour(x_mx, y_mx, density_mx, 5, colors="red", linestyles="dotted")
        plt.axis("equal")
        plt.show()


def test_kde_2d_gaussian_mixture_against_matlab():
    sample = np.loadtxt(
        fname=os.path.join(
            TEST_DATA_BASE_URL, "2d_sample%23gaussian_mixture%23matlab.txt"
        ),
        delimiter=",",
    )
    expected_density_mx = np.loadtxt(
        fname=os.path.join(
            TEST_DATA_BASE_URL, "2d_density%23gaussian_mixture%23matlab.txt"
        ),
        delimiter=",",
    )
    expected_x_mx = np.loadtxt(
        fname=os.path.join(
            TEST_DATA_BASE_URL, "2d_x_mesh%23gaussian_mixture%23matlab.txt"
        ),
        delimiter=",",
    )
    expected_y_mx = np.loadtxt(
        fname=os.path.join(
            TEST_DATA_BASE_URL, "2d_y_mesh%23gaussian_mixture%23matlab.txt"
        ),
        delimiter=",",
    )

    _, density_mx, x_mx, y_mx = kde_2d(
        sample_mx=sample, n_row_mx=2 ** 8, xy_min=[-10, -5], xy_max=[10, 5]
    )

    np.testing.assert_allclose(x_mx, expected_x_mx, atol=1e-3)
    np.testing.assert_allclose(y_mx, expected_y_mx, atol=1e-3)
    np.testing.assert_allclose(density_mx, expected_density_mx, atol=5e-3)

    if plot_fig is True:
        plt.subplots()
        plt.contour(expected_x_mx, expected_y_mx, expected_density_mx, 5)
        plt.contour(x_mx, y_mx, density_mx, 5, colors="red", linestyles="dotted")
        plt.show()


def test_kde_2d_sinusoidal_against_matlab():
    sample = np.loadtxt(
        fname=os.path.join(TEST_DATA_BASE_URL, "2d_sample%23sinusoidal%23matlab.txt"),
        delimiter=",",
    )
    expected_density_mx = np.loadtxt(
        fname=os.path.join(TEST_DATA_BASE_URL, "2d_density%23sinusoidal%23matlab.txt"),
        delimiter=",",
    )
    expected_x_mx = np.loadtxt(
        fname=os.path.join(TEST_DATA_BASE_URL, "2d_x_mesh%23sinusoidal%23matlab.txt"),
        delimiter=",",
    )
    expected_y_mx = np.loadtxt(
        fname=os.path.join(TEST_DATA_BASE_URL, "2d_y_mesh%23sinusoidal%23matlab.txt"),
        delimiter=",",
    )

    _, density_mx, x_mx, y_mx = kde_2d(
        sample_mx=sample, n_row_mx=2 ** 8, xy_min=[0, -2], xy_max=[1, 2]
    )

    np.testing.assert_allclose(x_mx, expected_x_mx, atol=1e-3)
    np.testing.assert_allclose(y_mx, expected_y_mx, atol=1e-3)
    np.testing.assert_allclose(density_mx, expected_density_mx, atol=5e-3)

    if plot_fig is True:
        plt.subplots()
        plt.contour(expected_x_mx, expected_y_mx, expected_density_mx, 5)
        plt.contour(x_mx, y_mx, density_mx, 5, colors="red", linestyles="dotted")
        plt.show()


def test_kde_2d_gaussian_against_scipy():
    """
    We compare only against a unimodal distribution as "bimodal or multi-modal
    distributions tend to be oversmoothed" by `scipy.stats.gaussian_kde`, i.e. it is
    not a reliable reference solution.
    """
    np.random.seed(42)
    sample = norm.rvs(size=(2000, 2))

    _, density_mx, x_mx, y_mx = kde_2d(sample_mx=sample)

    # scipy
    gkde = gaussian_kde(dataset=sample.T)
    xy_mx = np.hstack((x_mx.reshape(-1, 1), y_mx.reshape(-1, 1)))
    expected_density_mx = gkde.evaluate(xy_mx.T).reshape(x_mx.shape)

    np.testing.assert_allclose(density_mx, expected_density_mx, atol=1e-2)

    if plot_fig is True:
        plt.subplots()
        plt.contour(x_mx, y_mx, expected_density_mx, 5)
        plt.contour(x_mx, y_mx, density_mx, 5, colors="red", linestyles="dotted")
        plt.show()


def test_kde_2d_gaussian_with_weights_against_scipy():
    """
    We compare only against a unimodal distribution as "bimodal or multi-modal
    distributions tend to be oversmoothed" by `scipy.stats.gaussian_kde`, i.e. it is
    not a reliable reference solution.
    """
    np.random.seed(42)
    n_sample = 3000
    sample = norm.rvs(size=(n_sample, 2))
    weights = np.ones(n_sample)
    weights[0 : int(n_sample / 3)] = 0.5
    sample = sample[sample[:, 1].argsort()]

    _, density, x_mesh, y_mesh = kde_2d(sample_mx=sample, weight_vec=weights)
    _, density_nw, x_mesh_nw, y_mesh_nw = kde_2d(sample_mx=sample)

    # scipy
    gkde = gaussian_kde(dataset=sample.T, weights=weights)
    xy_mesh = np.hstack((x_mesh.reshape(-1, 1), y_mesh.reshape(-1, 1)))
    expected_density = gkde.evaluate(xy_mesh.T).reshape(x_mesh.shape)

    np.testing.assert_allclose(density, expected_density, atol=1e-2)

    if plot_fig is True:
        plt.subplots()
        plt.contour(x_mesh, y_mesh, expected_density, 5, colors="blue")
        plt.contour(x_mesh, y_mesh, density, 5, colors="red", linestyles="dotted")
        plt.contour(
            x_mesh_nw, y_mesh_nw, density_nw, 5, colors="green", linestyles="dotted"
        )
        plt.show()


def test_kde_2d_wrong_sample_input():
    with pytest.raises(ValueError):
        sample = np.random.random((5, 3))
        kde_2d(sample_mx=sample)


def test_kde_2d_wrong_weights_input():
    with pytest.raises(ValueError):
        sample = np.random.random((5, 2))
        kde_2d(sample_mx=sample, weight_vec=[1, 2])
    with pytest.raises(ValueError):
        sample = np.random.random((3, 2))
        kde_2d(sample_mx=sample, weight_vec=[-1, 2, 3])


# --------------------------------------------------------------------------------------
# 2D utilities
# --------------------------------------------------------------------------------------
def test_dct2d_idct2d():
    sample = np.random.randn(16, 16)
    np.testing.assert_allclose(idct2d(dct2d(sample)), sample)


def test_hist_2d_against_matlab():
    """
    Testing for 2d sample_vec as that's the one only needed in kde2d
    reference solution: kde2d.m function binned_sample=hist_2d(sample_vec,M)
    """
    x = [0.11, 0.21, 0.31, 0.31, 0.21]
    y = [0.61, 0.31, 0.91, 0.91, 0.31]
    sample = np.vstack((x, y)).T
    m = 5

    expected_binned_sample = np.array(
        [
            [0.0, 0.0, 0.0, 0.2, 0.0],
            [0.0, 0.4, 0.0, 0.0, 0.4],
            [0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 0.0],
        ]
    )

    binned_sample = hist_2d(sample, m)
    np.testing.assert_allclose(binned_sample, expected_binned_sample)
