import matplotlib.pyplot as plt
import numpy as np
import pytest

from taralli.loglikelihood_builder.utils import (
    correlation_function,
    correlation_matrix,
)

plot_fig = False


# ======================================================================================
# CORRELATION MATRIX
# ======================================================================================
def test_correlation_matrix():
    """
    Only testing the shape and basic properties of the matrix, the actual content is
    checked on the level of the correlation_function.
    """
    x_mx = np.array([[0, 1], [1, 0], [1, 1]])

    def correlation_func(d):
        return correlation_function(
            d=d, correlation_length=5, function_type="exponential"
        )

    rho_mx = correlation_matrix(x_mx, correlation_func=correlation_func)

    assert rho_mx.shape[0] == rho_mx.shape[1] == x_mx.shape[0]
    np.testing.assert_allclose(rho_mx, rho_mx.T)
    assert np.all(np.diag(rho_mx) == 1)
    assert np.min(rho_mx) <= 1
    assert np.min(rho_mx) >= 0


# ======================================================================================
# CORRELATION FUNCTION
# ======================================================================================
def test_correlation_function_left_right():
    d = np.linspace(start=0, stop=100, num=100)
    correlation_length = 5
    function_types = ["exponential", "cauchy", "gaussian"]
    rho = dict()
    for function_type in function_types:
        rho[function_type] = correlation_function(
            d=d, correlation_length=correlation_length, function_type=function_type
        )

    rho_left = [rho[function_type][0] for function_type in rho]
    rho_right = [rho[function_type][-1] for function_type in rho]

    np.testing.assert_allclose(rho_left, np.ones(len(function_types)))
    np.testing.assert_allclose(rho_right, np.zeros(len(function_types)), atol=1e-5)

    if plot_fig:
        plt.subplots()
        for function_type in function_types:
            plt.plot(d, rho[function_type], label=function_type)
        plt.legend()
        plt.show()


def test_correlation_function_limits():
    d = np.array([0, 1])
    function_types = ["exponential", "cauchy", "gaussian"]
    n_function_types = len(function_types)

    # ..................................................................................
    # Zero correlation length
    # ..................................................................................
    correlation_length = 0
    rho_expected = np.tile(np.array([1, 0]), (n_function_types, 1))

    rho = np.empty((n_function_types, d.shape[0]))
    for ii, function_type in enumerate(function_types):
        rho[ii, :] = correlation_function(
            d=d, correlation_length=correlation_length, function_type=function_type
        )

    np.testing.assert_allclose(rho, rho_expected)

    # ..................................................................................
    # Close to zero correlation length
    # ..................................................................................
    correlation_length = 1e-12
    rho_expected = np.tile(np.array([1, 0]), (n_function_types, 1))

    rho = np.empty((n_function_types, d.shape[0]))
    for ii, function_type in enumerate(function_types):
        rho[ii, :] = correlation_function(
            d=d, correlation_length=correlation_length, function_type=function_type
        )

    np.testing.assert_allclose(rho, rho_expected, atol=1e-10)

    # ..................................................................................
    # Infinite correlation length
    # ..................................................................................
    correlation_length = np.inf
    rho_expected = np.tile(np.array([1, 1]), (n_function_types, 1))

    rho = np.empty((n_function_types, d.shape[0]))
    for ii, function_type in enumerate(function_types):
        rho[ii, :] = correlation_function(
            d=d, correlation_length=correlation_length, function_type=function_type
        )

    np.testing.assert_allclose(rho, rho_expected)

    # ..................................................................................
    # "Close" to infinite correlation length
    # ..................................................................................
    correlation_length = 1e12
    rho_expected = np.tile(np.array([1, 1]), (n_function_types, 1))

    rho = np.empty((n_function_types, d.shape[0]))
    for ii, function_type in enumerate(function_types):
        rho[ii, :] = correlation_function(
            d=d, correlation_length=correlation_length, function_type=function_type
        )

    np.testing.assert_allclose(rho, rho_expected)


def test_correlation_function_wrong_d_input():
    with pytest.raises(ValueError):
        correlation_function(d=-1, correlation_length=1)


def test_correlation_function_wrong_correlation_length_input():
    with pytest.raises(ValueError):
        correlation_function(d=1, correlation_length=-1)
    with pytest.raises(TypeError):
        correlation_function(d=1, correlation_length=[1, 2])
