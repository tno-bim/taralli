# Import packages
import numpy as np
from scipy import stats

from taralli.loglikelihood_builder.base import (
    MeasurementSpaceTimePoints,
    log_likelihood_linear_normal,
)
from taralli.loglikelihood_builder.utils import correlation_function
from taralli.parameter_estimation.base import EmceeParameterEstimator


def test_task4():
    # Input data
    x_meas = np.array([[0.05, 0.035]])
    inertia = 1e-3  # second moment of inertia in m**4
    span = 10  # span in m
    load = 1e5  # point load in N

    # Expected values
    corr_mx_expected = np.array([[1, 0.5], [0.5, 1]])

    # Definition of the log_prior
    mu_0 = 30  # GPa
    sd_0 = 30  # GPa
    std_model = 0.01  # m

    def log_prior(theta):
        return stats.norm.logpdf(theta, mu_0, sd_0)

    # Physical model
    def beam_deflection(load, span, inertia, elastic_mod, a, d):
        """
        Computes the deflection of a simply supported beam under a point load.
        Args:
            load: point load intensity.
            span: length of the beam.
            inertia: second moment of inertia.
            elastic_mod: Young`s modulus.
            a: distance between the point load and the further extreme of the beam
                called "O".
            d: distance between the point where the displacement is calculated and "O".

        Returns:
            x_predicted: displacement.
        """
        b = span - a
        if a < d:
            raise Exception("a should be greater than or equal x")
        x_predicted = (
            load
            * b
            * d
            / (6 * 10 ** 9 * span * elastic_mod * inertia)
            * (span ** 2 - b ** 2 - d ** 2)
        )
        return x_predicted

    def physical_model_fun(theta):
        x1 = beam_deflection(
            load=load,
            span=span,
            inertia=inertia,
            elastic_mod=theta,
            a=span / 2,
            d=span / 2,
        )
        x2 = beam_deflection(
            load=load,
            span=span,
            inertia=inertia,
            elastic_mod=theta,
            a=span / 2,
            d=span / 4,
        )
        x_model = np.array([[x1, x2]])
        return x_model

    # Adding measurement points
    ms = MeasurementSpaceTimePoints()

    ms.add_measurement_space_points(
        coord_mx=[[span / 2, 1], [span / 4, 1]],
        group="translation",
        sensor_name="TNO",
        standard_deviation=std_model,
    )

    # Plot measurement points
    ms.plot_measurement_space_points()

    # Add correlation function
    def correlation_func_translation(d):
        return correlation_function(d, correlation_length=3.6)

    ms.add_measurement_space_within_group_correlation(
        group="translation", correlation_func=correlation_func_translation
    )

    cov_mx = ms.compile_covariance_matrix()

    # Correlation matrix
    corr_mx = ms.corr_mx

    # log_likelihood
    def log_likelihood_fun(theta):
        ll = log_likelihood_linear_normal(
            theta, x_meas=x_meas, physical_model=physical_model_fun, e_cov_mx=cov_mx
        )
        return ll

    # Bayesian inference
    nwalkers = 20
    stat_model = EmceeParameterEstimator(
        log_likelihood=log_likelihood_fun,
        log_prior=log_prior,
        ndim=1,
        sampling_initial_positions=45 + np.random.randn(nwalkers, 1),
        nwalkers=20,
        nsteps=5_000,
    )

    stat_model.estimate_parameters()
    stat_model.summary()

    np.testing.assert_allclose(corr_mx, corr_mx_expected, atol=0.01)
    np.testing.assert_allclose(stat_model.summary_output["mean"], 45.042, atol=1)
    np.testing.assert_allclose(
        np.sqrt(stat_model.summary_output["covariance"]), 9.383, atol=1
    )
