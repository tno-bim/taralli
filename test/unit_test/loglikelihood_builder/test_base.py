import pytest

from taralli.loglikelihood_builder.base import MeasurementSpaceTimePoints


# ======================================================================================
# CHECK INPUT
# ======================================================================================
def test_check_space_point_input_ndim():

    ms = MeasurementSpaceTimePoints()
    with pytest.raises(Exception):
        assert ms.add_measurement_space_points(coord_mx=[[2, 1, 3, 4], [5, 1, 1, 4]])


def test_check_space_point_input_group():
    ms = MeasurementSpaceTimePoints()
    with pytest.raises(Exception):
        assert ms.add_measurement_space_points(
            coord_mx=[[2, 1], [5, 1]], group=["strain", "displacement"]
        )


def test_check_space_point_input_sensor():
    ms = MeasurementSpaceTimePoints()
    with pytest.raises(Exception):
        assert ms.add_measurement_space_points(
            coord_mx=[[2, 1], [5, 1]], sensor=["Arpad", "pizza"]
        )


def test_check_space_point_input_model_unc_std():
    ms = MeasurementSpaceTimePoints()
    with pytest.raises(Exception):
        assert ms.add_measurement_space_points(
            coord_mx=[[2, 1], [5, 1], [6, 1]], model_unc_std=[0.1, 0.2]
        )


def test_check_space_point_input_meas_unc_std():
    ms = MeasurementSpaceTimePoints()
    with pytest.raises(Exception):
        assert ms.add_measurement_space_points(
            coord_mx=[[2, 1], [5, 1], [6, 1]], measurement_unc_std=[0.1, 0.2]
        )


def test_check_space_point_input_sensor_number():
    ms = MeasurementSpaceTimePoints()
    with pytest.raises(Exception):
        assert ms.add_measurement_space_points(
            coord_mx=[[2, 1], [5, 1], [6, 1]], sensor_number=[1, 5]
        )


def test_check_time_point_input_coord():
    ms = MeasurementSpaceTimePoints()
    with pytest.raises(Exception):
        assert ms.add_measurement_space_points(coord_mx=[[2, 1], [5, 1]])


def test_check_time_point_input_group():
    ms = MeasurementSpaceTimePoints()
    with pytest.raises(Exception):
        assert ms.add_measurement_space_points(
            coord_mx=[[2, 1], [5, 1]], group=["strain", "displacement"]
        )
