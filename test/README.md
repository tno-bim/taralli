## Guidance on writing tests

- to ease orientation, the test folder structure should mirror that of the tested code
- we use `pytest` for unit testing
- to run the test with coverage run this command from the root of the repository:
  `coverage run -m pytest && coverage report -m`
