# taralli changelog

## 0.8.4 (2021-May-31)

## Changed (3 items)

- Documentation:
  * switch from reST to MyST (extended markdown) to ease maintenance and contribution
  * switch to [furo](https://github.com/pradyunsg/furo) theme for better look
  * code snippets can be copied
  * small improvements of the documentation, e.g. fixing typos, removing empty sections
  * switch to a bibtex like referencing system for convenience
  * external links now automatically (javascript) open in a new tab, no need for raw
    html code
- `setup.py` and CI pipeline dependencies are harmonized (need to change only at one
  place: `setup.py`)
- Do not allow a major version number change in package dependencies needed for
  `taralli` users and generating the documentation
- Migration to gitlab.com and rename to `taralli`.

## Added (1 item)

- Documentation:
  - code snippets can be copied with a click

## 0.8.4 (2021-xx-xx)

### Changed (1 change)

- Change how the walkers' initial (starting) position is determined in
  `EmceeParameterEstimator`. Now all positions must be specified by the user, and it
  is the user's responsibility to provide reasonable values.
  * `sampling_initial_position` -> `sampling_initial_positions`, i.e. from a single
    starting point -> one starting point for each walker.
  * this is a breaking change.

### Fixed (1 change)

- Fix the issue with `EmceeParameterEstimator` walker initialization by putting the
  burden on the user to provide reasonable values. See also the corresponding
  change. #67, #73, #84


## 0.8.3 (2021-Febr-09)

### Changed (1 item)

- Matrix plot (1D and 2D marginals):
  * small aesthetic improvements, e.g. legend placement, axis labels
  * more customizable through input arguments: dimension labels, title, etc.
  * more extended testing (test Warning, test multiple dimensions, etc.)

### Fixed (1 item)

- Matrix plot (1D and 2D marginals):
  * credible region errors do not break the plotting anymore but only cause to not
      plotting the credible regions #69
  * pixelated 2D kernel density plots, the user now can control the resolution
    through #82
