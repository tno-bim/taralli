# Import packages
import os

import numpy as np
from scipy.stats import norm

from taralli.parameter_estimation.base import EmceeParameterEstimator

# Input data
x_measured = np.array([0.05])  # m
inertia = 1e-3  # second moment of inertia in m**4
length_beam = 10  # span in m
Q = 1e5  # point load in N

# Definition of the log_prior
mu_0 = 30  # GPa
sd_0 = 50  # GPa


def log_prior(theta):
    return norm.logpdf(x=theta, loc=mu_0, scale=sd_0)


# Definition of the log_likelihood
def log_likelihood(theta):
    sd_measured = 0.01
    x_predicted = Q * length_beam ** 3 / (48 * 1e9 * theta * inertia)
    return norm.logpdf((x_measured - x_predicted) / sd_measured)


# Create model
emcee_model = EmceeParameterEstimator(
    log_likelihood=log_likelihood,
    log_prior=log_prior,
    ndim=1,
    nwalkers=20,
    sampling_initial_positions=55 + np.random.randn(20, 1),
    nsteps=5_000,
)

# Plot prior
emcee_model.plot_prior()

# Estimate parameter
emcee_model.estimate_parameters()

# Inspect results
emcee_model.plot_posterior()
emcee_model.summary()

# Save fig
DIR_STATIC = "docs\\_static"
fname_prior = os.path.join(DIR_STATIC, "task3_prior_emcee.png")
fig_prior, _ = emcee_model.plot_prior()
fig_prior.savefig(fname=fname_prior)

fname_posterior = os.path.join(DIR_STATIC, "task3_posterior_emcee.png")
fig_posterior, _ = emcee_model.plot_posterior()
fig_posterior.savefig(fname=fname_posterior)
