# Import packages
import numpy as np
from scipy.stats import norm

from taralli.parameter_estimation.base import EmceeParameterEstimator

# Inputs
x_meas = np.array([9.9, 20, 30.1])
F = np.array([10, 20, 30])
std_meas = 0.1


# the physical model, theta -> x_pred
def physical_model(theta):
    return F * theta


# the loglikelihood function, theta -> log_like value
def log_likelihood(theta):
    x_pred = physical_model(theta)
    return norm.logpdf((x_meas - x_pred) / std_meas)


# Prior function
def log_prior(theta):
    return norm.logpdf(x=theta, loc=2, scale=10)


stat_model = EmceeParameterEstimator(
    log_likelihood=log_likelihood,
    log_prior=log_prior,
    ndim=1,
    nwalkers=10,
    sampling_initial_positions=3 + np.random.randn(10, 1) / 10,
)

stat_model.estimate_parameters()

stat_model.summary()

stat_model.plot_posterior()
