# Import packages

import pickle

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
from scipy import stats

from taralli.loglikelihood_builder.base import (
    MeasurementSpaceTimePoints,
    log_likelihood_linear_normal,
)
from taralli.loglikelihood_builder.utils import correlation_function
from taralli.parameter_estimation.base import EmceeParameterEstimator
from taralli.utilities.kde import kde_1d

# Input data
x_meas = np.array([[0.05, 0.035], [0.035, 0.028]])
intertia = 1e-3  # second moment of inertia in m**4
L = 10  # span in m
Q = 1e5  # point load in N


# Physical model
def beam_deflection(load, span, intertia, elastic_mod, a, d):
    """
    Computes the deflection of a simply supported beam under a point load.
    Args:
        load: point load intensity.
        span: length of the beam.
        intertia: second moment of inertia.
        elastic_mod: Young`s modulus
        a: distance between the point load and the further extreme of the beam called
            "O".
        d: distance between the point where the displacement is calculated and "O"

    Returns:
        x_predicted: displacement.
    """
    b = span - a
    if a < d:
        raise Exception("a should be bigger-equal than x")
    x_predicted = (
        load
        * b
        * d
        / (6 * 10 ** 9 * span * elastic_mod * intertia)
        * (span ** 2 - b ** 2 - d ** 2)
    )
    return x_predicted


def physical_model_fun(theta):
    x1 = beam_deflection(
        load=Q, span=L, intertia=intertia, elastic_mod=theta, a=L / 2, d=L / 2
    )
    x2 = beam_deflection(
        load=Q, span=L, intertia=intertia, elastic_mod=theta, a=L / 2, d=L / 4
    )
    x3 = beam_deflection(
        load=Q, span=L, intertia=intertia, elastic_mod=theta, a=3 * L / 4, d=L / 2
    )
    x4 = beam_deflection(
        load=Q, span=L, intertia=intertia, elastic_mod=theta, a=3 * L / 4, d=3 * L / 4
    )
    x_model = np.array([[x1, x2], [x3, x4]])
    return x_model


# log-prior
mu_0 = 30  # GPa
sd_0 = 30  # GPa
std_model = 0.01  # m


def log_prior(theta):
    return stats.norm.logpdf(theta, mu_0, sd_0)


# Adding measurement points
MS = MeasurementSpaceTimePoints()

MS.add_measurement_space_points(
    coord_mx=[[L / 2, 1], [L / 4, 1]],
    group="translation",
    sensor_name="TNO",
    standard_deviation=0.01,
)

MS.add_measurement_time_points(
    coord_vec=[1, 3.5],
    group="truckload",
)

# Plot measurement points
MS.plot_measurement_space_points()


# Add correlation function
def correlation_func_translation(d):
    return correlation_function(d, correlation_length=3.6)


def correlation_func_truck_load(d):
    return correlation_function(d, correlation_length=3.6)


MS.add_measurement_space_within_group_correlation(
    group="translation", correlation_func=correlation_func_translation
)
MS.add_measurement_time_within_group_correlation(
    group="truckload", correlation_func=correlation_func_truck_load
)

cov_mx = MS.compile_covariance_matrix()

# Correlation matrix
corr_mx = MS.corr_mx

# Plot correlation matrix
_, ax = plt.subplots()
plt.imshow(corr_mx)
plt.colorbar()
plt.show()


# log_likelihood
def log_likelihood_fun(theta):
    return log_likelihood_linear_normal(
        theta, x_meas=x_meas, physical_model=physical_model_fun, e_cov_mx=cov_mx
    )


# Bayesian inference
stat_model = EmceeParameterEstimator(
    log_likelihood=log_likelihood_fun,
    log_prior=log_prior,
    ndim=1,
    sampling_initial_positions=45 + np.random.randn(20, 1),
    nwalkers=20,
    nsteps=5_000,
)

stat_model.estimate_parameters()
stat_model.plot_posterior()

# pickle
pickle_in = open("task4.pickle", "rb")
task_4_results = pickle.load(pickle_in)
bandwidth4, density_vec4, x_vec4 = kde_1d(task_4_results)

# prior sample
stat_model.plot_prior(prob_mass_for_credible_region=0)
sample_prior = stat_model.prior_sample
bandwidth_pr, density_vec_pr, x_vec_pr = kde_1d(sample_prior)

# Plot
fig_data, _ = stat_model.plot_posterior(
    prob_mass_for_credible_region=0, display_legend=False
)
fig, axes = fig_data
axes[0, 0].plot(x_vec4, density_vec4, color="red")
axes[0, 0].plot(x_vec_pr, density_vec_pr, color="green", label="Prior distribution")

colors = ["#08519c", "red", "green"]
lines = [Line2D([0], [0], color=c, linewidth=3) for c in colors]
labels = ["Two sensors and two load cases", "Two sensors", "Prior distribution"]
fig.legend(lines, labels, loc="upper center")
