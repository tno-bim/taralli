# Import packages
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

from taralli.loglikelihood_builder.base import (
    MeasurementSpaceTimePoints,
    log_likelihood_linear_normal,
)
from taralli.loglikelihood_builder.utils import correlation_function
from taralli.parameter_estimation.base import EmceeParameterEstimator

# Input data
x_meas = np.array([[0.05, 0.035]])
inertia = 1e-3  # second moment of inertia in m**4
L = 10  # span in m
Q = 1e5  # point load in N

# Definition of the log_prior
mu_0 = 30  # GPa
sd_0 = 30  # GPa
std_model = 0.01  # m


def log_prior(theta):
    return stats.norm.logpdf(theta, mu_0, sd_0)


# Physical model
def beam_deflection(load, span, inertia, elastic_mod, a, d):
    """
    Computes the deflection of a simply supported beam under a point load.
    Args:
        load: point load intensity.
        span: length of the beam.
        inertia: second moment of inertia.
        elastic_mod: Young`s modulus.
        a: distance between the point load and the further extreme of the beam called
            "O".
        d: distance between the point where the displacement is calculated and "O".

    Returns:
        x_predicted: displacement.
    """
    b = span - a
    if a < d:
        raise Exception("a should be bigger-equal than x")
    x_predicted = (
        load
        * b
        * d
        / (6 * 10 ** 9 * span * elastic_mod * inertia)
        * (span ** 2 - b ** 2 - d ** 2)
    )
    return x_predicted


def physical_model_fun(theta):
    x1 = beam_deflection(
        load=Q, span=L, inertia=inertia, elastic_mod=theta, a=L / 2, d=L / 2
    )
    x2 = beam_deflection(
        load=Q, span=L, inertia=inertia, elastic_mod=theta, a=L / 2, d=L / 4
    )
    x_model = np.atleast_2d([x1, x2]).reshape((1, 2, -1))
    return x_model


# Adding measurement points

MS = MeasurementSpaceTimePoints()

MS.add_measurement_space_points(
    coord_mx=[[L / 2, 1], [L / 4, 1]],
    group="translation",
    sensor_name="TNO",
    standard_deviation=std_model,
)

# Plot measurement points
MS.plot_measurement_space_points()


# Add correlation function
def correlation_func_translation(d):
    return correlation_function(d, correlation_length=3.6)


MS.add_measurement_space_within_group_correlation(
    group="translation", correlation_func=correlation_func_translation
)

cov_mx = MS.compile_covariance_matrix()

# Correlation matrix
corr_mx = MS.corr_mx

# Plot correlation matrix
fig, ax = plt.subplots()
plt.imshow(corr_mx)
plt.colorbar()
plt.show()


# log_likelihood
def log_likelihood_fun(theta):
    return log_likelihood_linear_normal(
        theta,
        x_meas=x_meas,
        physical_model=physical_model_fun,
        e_cov_mx=cov_mx,
    )


# Bayesian inference
stat_model = EmceeParameterEstimator(
    log_likelihood=log_likelihood_fun,
    log_prior=log_prior,
    ndim=1,
    sampling_initial_positions=45 + np.random.randn(20, 1),
    nwalkers=20,
    nsteps=5_000,
)

stat_model.estimate_parameters()
stat_model.plot_posterior()

# Save fig
# import os
#
# DIR_STATIC = "docs\\_static"
# fname_posterior = os.path.join(DIR_STATIC, "task4_posterior_emcee.png")
# fig_posterior, range = stat_model.plot_posterior()
# fig_posterior.savefig(fname=fname_posterior)
#
# fname_ms_space = os.path.join(DIR_STATIC, "ms_space_task4.png")
# fig_ms_space = MS.plot_measurement_space_points()
# fig_ms_space.savefig(fname=fname_ms_space)
#
# fname_corr= os.path.join(DIR_STATIC, "corr_mx_task4.png")
# fig_corr, ax = plt.subplots()
# plt.imshow(corr_mx)
# plt.colorbar()
# plt.show()
# fig_corr.savefig(fname=fname_corr)
#
# # pickle
#
# task_4_results = stat_model.posterior_sample
# pickle_out = open("../task4.pickle", "wb")
# pickle.dump(task_4_results, pickle_out)
# pickle_out.close()
