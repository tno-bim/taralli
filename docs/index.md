# taralli

A python 🐍 package
* _to_ calibrate engineering models
* _by_ estimating their parameters and uncertainties
* _using_ data, computer models, and Bayesian statistics.

```{important}
   This package and its documentation is not an educational tool/material. Only
   those implementation independent concepts are discussed that are relevant for the
   implementation. It is the user's responsibility to have a good understanding of
   the implementation independent concepts (see {ref}`sec:prerequisites`). On the other
   hand, it is the developers' responsibility to make it clear what is implemented
   and make the package easy to use and intuitive. So if you have suggestions
   related to the latter please open a [GitLab issue][gitlab_issue] 💡 🐛.
```

[gitlab_issue]: https://gitlab.com/tno-bim/taralli/issues


```{toctree}
---
hidden:
---

package_overview
installation
crash_course
getting_started
module_overview
methods
api
user_faq
glossary
references
```

```{toctree}
---
hidden:
maxdepth: 1
caption: Development
---

for_contributors
```
