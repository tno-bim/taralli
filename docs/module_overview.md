(sec:module_overview)=
# Overview of modules

📚

This section:

* gives a general, high level overview of the `taralli` package and its modules;
* contains the basics of the mathematical background of each module;
* focuses on the inputs, outputs, and communication between modules;
* lists the scope and limitations of the modules.

## Overview

```{mermaid} _static/package_overview.mmd
```

| Module name                       | What it does                                                               | Math                                                                                                                            |
|-----------------------------------|----------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| {ref}`sec:parameter_estimation`   | Estimates the posterior distribution of $\boldsymbol{\theta}$.             | $p(\boldsymbol{\theta }\mid{\bf{x}}) = \frac{p({{\bf{x}}\mid\boldsymbol{\theta }) \cdot p(\boldsymbol{\theta })}}{{p(\bf{x})}}$ |
| {ref}`sec:log_likelihood_builder` | Helps to keep your sanity while building complex log-likelihood functions. | $\ln p({{\bf{x}}\mid\boldsymbol{\theta }}),\;{\mathbb{R}^K} \to {\mathbb{R}^1}$                                                 |
| {ref}`sec:surrogating`            | Replaces an expensive target function with a cheap to evaluate one(s).     | $f_{\rm{surrogate}}({\boldsymbol{\theta }})={\bf{x}},\;{\mathbb{R}^K} \to  {\mathbb{R}^{T \times S}}$                           |
| {ref}`sec:reliability_analysis`   | Estimates rare event probability.                                          | ${P_{\rm{f}}} = \int\limits_{g({\bf{Z}}) < 0} {p({\bf{z}}) \cdot {\rm{d}}{\bf{z}}}$                                             |

(sec:parameter_estimation)=
## Parameter estimation

The module deals solely with the following equation:

$$
p(\boldsymbol{\theta }|{\bf{x}}) = \frac{p({{\bf{x}}|\boldsymbol{\theta }) \cdot p(\boldsymbol{\theta })}}{{p(\bf{x})}}
$$ (eq:bayes_rule)

where

* $\bf{x}$: measurements, ${[T \times S]}$;
* $\boldsymbol{\theta}$: parameters of interest, ${[1 \times K]}$;
* $p({\boldsymbol{\theta}} | {\bf{x}})$: posterior distribution, ${\mathbb{R}
  ^K} \to {\mathbb{R}^1}$;
* $p({\bf{x}} | {\boldsymbol{\theta}})$: likelihood function, ${\mathbb{R}^{T \times S}}
  \to {\mathbb{R}^{1}}$;
* $p({\boldsymbol{\theta}})$: prior function, ${\mathbb{R}^K} \to {\mathbb{R}^
  {1}}$;
* $p({\bf{x}})$: evidence.

It provides a unified interface to computational methods that:

* estimate the posterior distribution, $p({\boldsymbol{\theta}} | {\bf{x}})$;
* estimate the evidence (not all methods), $p({\bf{x}})$.

Additionally, it has utility functions for visualization and general postprocessing
(see {ref}`sec:getting_started` and {ref}`sec:api` for further details).

(tab:bayes_computational_methods)=
```{list-table} Available computational methods for Bayesian inference.
---
header-rows: 1
---

* - Class
  - Package
  - $p({\boldsymbol{\theta}} | {\bf{x}})$
  - $p({\bf{x}})$
* - Nested sampling (static)
  - [nestle](https://github.com/kbarbary/nestle)
  - weighted sample
  - yes
* - Nested sampling (static and dynamic)
  - [dynesty](https://github.com/joshspeagle/dynesty)
  - weighted sample
  - yes
* - Ensemble sampling
  - [emcee](https://github.com/dfm/emcee)
  - sample
  - no
```

### Module interface

To aid the understanding of how this module is structured and can communicate with
other modules or external tools, we provide a brief description of the essential
inputs and outputs (see the {ref}`sec:api` for further details).

Essential inputs:

* `x`: measurements; `[TxS]` matrix (numpy array)
* `log_likelihood_fun`: log-likelihood function; function
    - input: `theta`, parameters of interest; `K` element vector (numpy array)
    - output: `log_like`, loglikelihood value; scalar
* `transformed_prior_fun`: transformed version of the prior distribution; function
    - input: `theta`
    - output: `p`, transformed prior density; a scalar (the transformation depends
        on the selected computational method, see
        {ref}`tab:bayes_computational_methods`).


Essential outputs:

* `sample`, a sample from the posterior of `theta`; `NxS` matrix (numpy array)
* `sample_weights`, weights corresponding to the `sample`; `NxS` matrix (numpy
    array)

(sec:log_likelihood_builder)=
## Log-likelihood builder

This module's goal is to help the construction of the `log_likelihood_fun` for the
{ref}`sec:parameter_estimation` module.

Measurement points:

* space point, e.g. a strain or translation sensor
* time point, e.g. construction stages, load cases

### Covariance/correlation matrix

Compilation convention for the correlation matrix:

* The space variables change faster than the time variables.
* The order is determined by the order of definition/adding to the model.
* The time and space correlation coefficients are multiplied.

```{image} _static/correlation_mx_compilation.png
---
height: 500px
align: center
alt: correlation_mx_compilation
---
```

Current limitations of the implementation (not that of its design):

* only within group correlation is considered
* the time correlation is insensitive to the space groups (applied to all space point
    sensors)


(sec:surrogating)=
## Surrogating

```{caution}
The description will be added later.
```

(sec:reliability_analysis)=
## Reliability analysis

```{caution}
The module and description will be added later.
```

* Not a replacement of traditional reliability methods but complementary.
* It takes advantage of the special sampling strategy of nested sampling approaches.
    This means that it is only usable if nested sampling is used for the parameter
    estimation.
