(sec:crash_course)=
# Crash course

⚡

This section gives you a quick start with `taralli`.

If you want to use the package as fast possible this is your section.

* First, {ref}` install <sec:installation>` the package.
* Go through the minimal examples or select the one that fits your need (for first
    time user we recommend going through sequentially all the minimal examples).

Let's get started! 🚀

## Estimation of a non-directly observed parameter using emcee for a simple case

We are going to solve [Task 3][task_3] from our internal Bayesian statistics learning
material.


[task_3]: https://docs.google.com/document/d/1vrzjcl8k1RoBaWocgTvVfLLG6LDAHiMwh_MC9PcnfsQ/edit#bookmark=id.ii2h4ua93iux


Import the required packages including `taralli`.

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Import packages"
end-before: "# Input data"
---
```

Let's define the parameters given in the description of the task.

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Input data"
end-before: "# Definition of the log_prior"
---
```

Define the natural logarithm of the prior function.

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Definition of the log_prior"
end-before: "# Definition of the log_likelihood"
---
```

Define the natural logarithm of the likelihood function.

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Definition of the log_likelihood"
end-before: "# Create model"
---
```

With the above we have uniquely defined the mathematical problem. Note that until
this point we haven't used `taralli`. Let's use it then to connect all the
components using the `EmceeParameterEstimator`:

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Create model"
end-before: "# Plot prior"
---
```

Now we are ready to estimate the parameter of interest (`E`):

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Estimate parameter"
end-before: "# Inspect results"
---
```


To inspect the results run the following codes:

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Inspect results"
end-before: "# Save fig"
---
```

```{image} _static/task3_posterior_emcee.png
:alt: posterior
:width: 300px
:align: center
```

We can also visualize a sample from the prior distribution:

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Plot prior"
end-before: "# Estimate parameter"
---
```

```{image} _static/task3_prior_emcee.png
:alt: prior
:width: 300px
:align: center
```

Congratulations! 🥳 You just completed the crash course on `taralli`. The herein
presented simple interface is the base of the entire package. We provide a lot more
functionalities to adjust the analysis (currently defaults are used) and to ease the
modelling process, e.g. building the likelihood function and post-processing the
results.

To see some of these functionalities see {ref}`sec:getting_started` or dive deep into
the details of the {ref}`sec:methods`.

### Complete code

```{literalinclude} examples/crash_course_emcee.py
---
language: python
start-after: "# Import packages"
end-before: "# Save fig"
---
```
