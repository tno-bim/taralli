(sec:user_faq)=
# Frequently asked questions

🙋

```{glossary}
    :sorted:

What does it mean that a likelihood function is comparably expensive to evaluate as to simulate from?
    It specifies the scope of the package. The following parameter
    estimation (inference) problems are not addressed: where the likelihood function
    is practically intractable to evaluate. These problems are solvable but require
    methods where the likelihood function is not evaluated but drawing of samples
    from the data generating process is feasible (the methods concerning these
    problems belong to the [ABC methods][abc]). `taralli` solely focuses on
    problems where the evaluation of the likelihood function is practically
    tractable.


How can I help/contribute?
    If you have suggestions please contact one of the developers, best done via
    [GitLab issues][issues]. If you would like to contribute to the code see
    {ref}`sec:for_contributors`.


What is the airspeed velocity of an unladen swallow?
    "Although a definitive answer would of course require further measurements,
    published species-wide averages of wing length and body mass, initial Strouhal
    estimates based on those averages and cross-species comparisons, the Lund wind
    tunnel study of birds flying at a range of speeds, and revised Strouhal numbers
    based on that study all lead me to estimate that the average cruising airspeed
    velocity of an unladen European Swallow is roughly 11 meters per second, or 24
    miles an hour." {cite}`corum2003`

```

[issues]: https://gitlab.com/tno-bim/taralli/-/issues

[abc]: https://en.wikipedia.org/wiki/Approximate_Bayesian_computatio
