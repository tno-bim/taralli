(sec:methods)=
# Methods

📔

This section describes the methods down to the implementation details.
It is not meant to be a textbook hence it will explain general concepts only if that is
essential, otherwise references are provided to the literature. The goal is to
provide description that is one level higher than the implementation but not higher.
For the documentation of the custom classes and functions see the {ref}`sec:api`.

```{caution}
🏗️ This is a work in progress section.
```

```{include} methods/computational_bayesian_inference.md
```
