(sec:package_overview)=
# Package overview

## Overview

`taralli` is a python 🐍 package
* _to_ calibrate engineering models
* _by_ estimating their parameters and uncertainties
* _using_ data, computer models, and Bayesian statistics.

With particular focus on:
* ease of use even with basic knowledge on statistics (undergraduate civil engineering
  statistics),
* manageable runtimes even for computationally demanding {term}`likelihood`
  functions (physics-based models).

Scoped to
* at maximum about 50 estimated parameters.
* {term}`likelihood` functions that are {term}`comparably expensive to evaluate as to simulate from <What does it mean that a likelihood function is comparably expensive to evaluate as to simulate from?>`.

(sec:prerequisites)=
## Recommended prerequisites

* Basic usage:
   - Python: if you used  `numpy` and/or `scipy` in total for at least 20 hours over
     the last six months then you should be covered.
   - Theory: basic knowledge of the underlying concepts:
       * Bayesian statistics: if you are familiar with the concepts in
         [this introduction to Bayesian statistics guide][bayes_intro] and can/could
         solve most of the problems then you are covered.
       * Surrogating: if you are familiar with the concept of surrogate modelling,
         fitting functions to points using optimization, and the basics of Gaussian
         processes then you are good.
       * Reliability analysis: the very basics of structural reliability (computing
         failure probabilities) and limit state design concepts. Familiarity with
         the crude Monte Carlo method.
   - Numerical methods: Basic (at least on the surface) understanding of the used
      computational methods in order to know what inputs and outputs to provide and
      expect. Although we provide sensible default values for many algorithms it is
      inevitable that you will need to look into their details to use them confidently.
* Advanced usage:
   - Python: you can benefit from reading the source code, this requires familiarity
      with classes and modules.
   - Theory: the more the better. A solid understanding of all related concepts is
      an advantage.
   - Numerical methods: it is essential that you understand the used numerical
     methods, so you can set the parameters to get better results faster, and
     probably most importantly to troubleshoot errors.


(sec:other_versions)=
## Other documentation versions

The latest version of each branch is used to build a documentation site 📖:

* [master branch](https://gitlab.com/tno-bim/taralli)
* [all other branches](https://gitlab.com/tno-bim/taralli/-/environments)

[bayes_intro]: https://docs.google.com/document/d/1vrzjcl8k1RoBaWocgTvVfLLG6LDAHiMwh_MC9PcnfsQ/edit
