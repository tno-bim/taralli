(sec:for_contributors)=
# For contributors

💻

This section describes:

* the main programming related decisions;
* how to contribute to the repository.

```{caution}
🏗️ This is a work in progress section that contains just notes at the moment.
```

## Design principles

The goals of the package:

* Easy to use yet powerful.
* Speeds up performing parameter estimation tasks considerably.
* Physical models (evaluator functions) can be plugged in.
* Modular: easy to extend, e.g. new sampling or surrogating algorithms added.


* Design for the user interface.
* As general as possible.
* Modular structure.
* Minimal and not package specific communication between modules, e.g. use the
    simplest possible python variables to communicate between modules to make them
    reusable.


* Take inspiration from widely used python packages in terms user interface and under
    hood structure, e.g.

  - sckitlearn
  - keras

* Take inspiration from python packages with similar structure (the user brings
    his/her model), e.g.

  - emukit

* Take inspiration from packages with statistical comparable goal (parameter
    estimation):

  - nestle
  - dynesty
  - pyMC3
  - edward
  - stan
  - emcee
  - [cobaya](https://cobaya.readthedocs.io/en/latest/index.html)

* Visualization
  - corner
  - GetDist

## Coding conventions

We follow/use the following conventions and practices:

* [PEP8](https://www.python.org/dev/peps/pep-0008/).
* [black](https://github.com/psf/black).
* [type hinting](https://docs.python.org/3/library/typing.html).
* [Google docstring](https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings).

Please adhere to them. If the provided code does not follow these recommendations
then please fix it.

## Branches

The ``master`` branch is protected. Please do all development work on branches other
than the ``master`` and merge them to the ``master`` when you are finished.

## Packaging

We're using a private pypi server (running [pypiserver](https://github.com/pypiserver/pypiserver)) to distribute the stable
version of the package.

The recommended workflow:

Make a local package build:

```bash
python setup.py sdist bdist_wheel
```

Check the build:

```bash
twine check dist\<build file>
```


Upload to the server (registration is needed):

```bash
twine upload --repository-url https://pypi-server-ubuntu-2-0.westeurope.cloudapp.azure.com/ dist/*
```

The server is password protected, so you need to have a registered username to be able
to upload. The server is managed by Arpad Rozsas, you can request a username and
password from him.

The server is hosted in the cloud service: Microsoft Azure and access is restricted
to TNO IP addresses, i.e. only accessible from within the TNO intranet.
This is achieved by adding the TNO IP address ranges to the inbound rules. If you
cannot download the package from the TNO intranet please contact us and share you
public IP as well, so we can extend the range of IPs.

With a `.pypirc` file:

The upload step changes to this:

```bash
twine upload -r azurepypi dist/*
```

`.pypirc`

```
[distutils]
index-servers=
    azurepypi

[azurepypi]
repository=https://pypi-server-ubuntu-2-0.westeurope.cloudapp.azure.com
username=<your_username>
password=<your_password>
```

## Unit test

* We use [pytest](https://github.com/pytest-dev/pytest) for unit testing.
* Over time, we would like to add testing for all major functionalities.
* If you add to the code base, please also implement the corresponding unit tests.
* Large files (~>100 KB) that are needed for testing or for the examples should not
    be added to the repo.

    - Currently, these files are hosted in a public AWS S3 bucket, from where they are
        downloaded when needed.
    - git LFS seems to be a better solution, but it does not work on the TNO DV GitLab
        server.


% ## Code coverage

% To be added.

## Documentation

* [Sphinx](https://www.sphinx-doc.org/en/master/) and [MyST](https://github.com/executablebooks/MyST-Parser) are used for the
  documentation.

* Code documentation:

  - Besides, the general descriptions and guides the python code is automatically
    documented: the code is pulled from docstrings.
  - If you add any new class or function please add a docstring and adhere to the
    conventions used in the rest of the code.

* We automatically generate the documentation from the latest version of each branch:

    - [master branch][master_doc];
    - [all other branches][non_master_doc].

* The documentation generated from the [master branch][master_doc] is made available
  via GitLab Pages, while the other branches are made accessible via a [deploy and artifact trick](https://stackoverflow.com/a/58402821/4063376).
  Once GitLab supports [multi-version documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/16208)
  we should migrate to that solution.

[master_doc]: https://tno-bim.gitlab.com/taralli

[non_master_doc]: https://gitlab.com/tno-bim/taralli/-/environments
