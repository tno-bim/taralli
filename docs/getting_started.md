(sec:getting_started)=
# Getting started

🏃

This section gives you a more thorough overview of the features of the package
through simple examples.

It is a natural step after completing the {ref}`sec:crash_course` and also a good
reference to find a minimal example for starting the implementation of your problem.

If you need more control see the {ref}`sec:methods` and/or the {ref}`sec:API`.

```{caution}
🏗️ This is a work in progress section and many examples are missing.
```

% ## Parameter estimation interface

% ### Changing the solver

% ## Estimation of a non-directly observed parameter

% ## Using an external software to evaluate the physical model

% ## Surrogating the physical model

%```{note}

% The surrogating  module is still under construction, thank

% you for your patience!

% ```

## Constructing complex likelihood functions

We are going to solve [Task 4][task_4] from our internal Bayesian statistics learning
material.


[task_4]: https://docs.google.com/document/d/1vrzjcl8k1RoBaWocgTvVfLLG6LDAHiMwh_MC9PcnfsQ/edit#bookmark=id.ii2h4ua93iux


To solve the task we start importing all the necessary packages.

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Import packages"
end-before: "# Input data"
---
```

We define the parameters given in the description of the task.

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Input data"
end-before: "# Definition of the log_prior"
---
```

Define the natural logarithm of the prior function

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Definition of the log_prior"
end-before: "# Physical model"
---
```

and the physical model:

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Physical model"
end-before: "# Adding measurement points"
---
```

In this task the vertical displacement is measured at two different locations of the
beam. We need to build a log-likelihood function able to take this additional
information into account. The module `loglikehood_builder` of `taralli` can
help us in this phase.

We start assembling the covariance creating an instance of the class
`MeasurementSpaceTimePoints` and adding the information related to the measurement
points.

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Adding measurement points"
end-before: "# Plot measurement points"
---
```

To check that the information provided is correct we can plot the spatial
distribution of measurement points.

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Plot measurement points"
end-before: "# Add correlation function"
---
```

```{image} _static/ms_space_task4.png
:height: 300px
:width: 400 px
:align: center
:alt: spatial distribution measurement points
```

Now, we can assign to our model a within-group correlation function and compile the
covariance matrix.

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Add correlation function"
end-before: "# Correlation matrix"
---
```

As an additional check, before defining the log-likelihood function, it is possible to
plot the correlation matrix:

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Correlation matrix"
end-before: "# log_likelihood"
---
```

```{image} _static/corr_mx_task4.png
:height: 300px
:align: center
:alt: correlation matrix
```

Now we can finally define the log-likelihood function:

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# log_likelihood"
end-before: "# Bayesian inference"
---
```

and run the Bayesian inference.

```{literalinclude} examples/example_task4.py
---
language: python
start-after: "# Bayesian inference"
end-before: "# Save fig"
---
```

```{image} _static/task4_posterior_emcee.png
:height: 300px
:align: center
:alt: posterior
```

You may wonder how the posterior distribution will look like if we have
**two additional measurements concerning a load in a different position**, as
illustrated in the following figure:

```{image} _static/task45.png
:align: center
:alt: beam
```

With some small modifications to our previous code we can easily check it.

We modify the input, adding the new measurements.

```{literalinclude} examples/example_task_45_combined.py
---
language: python
start-after: "# Input data"
end-before: "# Physical model"
---
```

We update the physical model.

```{literalinclude} examples/example_task_45_combined.py
---
language: python
start-after: "# Physical model"
end-before: "# log-prior"
---
```

We add the new information regarding the measurement space points, and the measurement
time points.

```{literalinclude} examples/example_task_45_combined.py
---
language: python
start-after: "# Adding measurement points"
end-before: "# Plot measurement points"
---
```

We assign a within-group time correlation function and compile the covariance matrix.

```{literalinclude} examples/example_task_45_combined.py
---
language: python
start-after: "# Add correlation function"
end-before: "# Correlation matrix"
---
```

Now, we are ready to define the log-likelihood function and perform the Bayesian
inference.

We plot the posterior distribution and on top we plot the posterior of the previous
case and the prior. As expected, adding information, the posterior becomes more
informative!

```{literalinclude} examples/example_task_45_combined.py
---
language: python
start-after: "# log_likelihood"
end-before: "# pickle"
---
```

```{image} _static/comparison_task45.png
:height: 300px
:align: center
:alt: beam
```

% ## Reliability analysis

% ```{note}
% The reliability analysis  module is still under construction, thank
% you for your patience!
% ```

% ## Putting everything together

% In this example we will put all the pieces together.
