(sec:glossary)=
# Glossary

🗃️

This page provides a description of some terms used in this documentation.

The goal is to it make as clear and concrete as possible what is meant by certain terms.
This is particularly important for terms that have multiple usages in the literature.

```{glossary}
    :sorted:

taralli
    delicious toroidal Italian snack foods, really loved by the developers of
    this library.

Bayesian statistics
    is one of the two main statistical paradigms (frequentist is the other one).
    Probability in Bayesian statistics reflects the analyst's current state of
    knowledge using all available information ("degree of belief"[^belief]) and it
    bases inference on the relative evidence of the parameter values given a dataset
    {cite}`spiegelhalter2009`). It is also regarded as the extension of classical
    logic {cite}`jaynes2003`.


credible interval (or posterior interval)
    a Bayesian uncertainty interval that expresses parameter estimation
    uncertainty due to sampling variability. A $p$-level credible interval
    of a random variable is an interval that contains 90% probability mass of the
    distribution of the random variable. This means that the real value of the
    parameter is within the $p$-level credible interval with $p$ probability.


evidence
    probability of observing the data ($\boldsymbol{x}$) given a model
    ($\cal M$). Obtained by integrating the likelihood function over the
    entire parameter space ($\boldsymbol{\Theta}$).


likelihood
    expresses how likely is the data ($\boldsymbol{x}$) given a set of
    parameter values ($\boldsymbol{\theta}$).


measurement uncertainty
    statistical model: white noise, a random variable for each sensor that has
    *i.i.d.* realization upon each measurement. Any two sensors' measurement
    uncertainty realizations are assumed to be independent.


model ($\cal M$)
    a mathematical representation of selected characteristics of an object or
    phenomenon.


model uncertainty
    a type of uncertainty that stems from the mismatch between reality and
    corresponding model (even under deterministic settings). It can be attributed
    to: (*i*) parameters which influence the real behaviour but not considered in
    the model, e.g. spatial variability of concrete strength is not considered in
    the model; (*ii*) the mathematical representation of the model does not
    corresponds to reality, e.g. second order polynomial instead of sinusoidal.
    The effect of these approximations is looked at as uncertainty (unknown
    underlying mechanism, i.e. not captured by the model). We differentiate
    between physical and probabilistic model uncertainty but only the former is
    the subject of this report.


physical model ($\cal M_\mathrm{Ph}$)
    a deterministic model which describes/represents a physical phenomenon.
    Note that it can be empirical, first principles based, analytical (symbolic),
    numerical, etc. Provided with the same inputs it always yields the same
    outputs. Examples of physical models: (*i*) a standardized, symbolic formula
    to calculate the shear resistance of a reinforced concrete beam; (*ii*) a
    nonlinear finite element *model*.


posterior
    a distribution that expresses the analyst knowledge after seeing the data
    (combined knowledge from the prior and data).


prior
    a distribution that expresses the analyst knowledge before seeing the data.
    Unless otherwise stated, a uniform distribution is assumed.


probabilistic model ($\cal M_\mathrm{Pr}$)
    a model which describes/represents uncertainties. Here, we consider only
    approaches which comply with probability theory, e.g. fuzzy models are excluded.


statistical inference
    "the process of drawing conclusions about populations or other collections of
    objects about which we have only partial knowledge from samples" {cite}`simon1997`.

```

[^belief]: It is often termed subjective probability; however, subjective here is not
    used in its everyday meaning. Instead, it reflects the analyst’s inevitable
    modelling assumptions and decisions, which are not solely based on the
    information conveyed by the data but on his or her expert judgment. Still, the
    rationality and consistency are maintained: two analysts with the same experience
    and information would come up with the same inference.
