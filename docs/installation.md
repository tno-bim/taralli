(sec:installation)=
# Installation

🥚 🐣 🐥

```{caution}
The project is still in a development stage 🏗️ and we do not yet recommend to use
it in projects unless you are a developer or the developers are supporting your
work.
```

% ## pip

% You can install the most recent stable version by using:

% ```bash
% pip install taralli
% ```

## From the source

Clone the [repository][repository] and install the package locally using (run from the
root of the repository):

````{tab} User
For users who only would like to use the package (and do not want to contribute to the
development):

```bash
pip install -e .
```
````

````{tab} Developer
For developers:
```bash
pip install -e .[development]
```
````


[repository]: https://gitlab.com/tno-bim/taralli
