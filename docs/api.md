(sec:api)=

# API

📝

This page details the models, classes, and methods provided by the `taralli`
package.

```{caution}
🏗️ This is a work in progress experimental section. At the we inlcude the
documentation of only a few modules for testing. See the source code and the
documentation there for further details on the API.
```

## Top-level interface

## Bayesian parameter estimation

```{eval-rst}
.. automodule:: taralli.parameter_estimation.base
    :members:
    :undoc-members:
    :show-inheritance:
```

### Post-processing

```{eval-rst}
.. automodule:: taralli.parameter_estimation.visualization
    :members:
    :undoc-members:
    :show-inheritance:
```


## Likelihood builder

```{eval-rst}
.. automodule:: taralli.loglikelihood_builder.base
    :members:
    :undoc-members:
    :show-inheritance:
```

## Surrogate modelling

### Post-processing

## Reliability analysis

### Post-processing
